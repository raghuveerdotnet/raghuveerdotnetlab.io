---
layout: post
title: What does it mean to be truly educated?
comments: true
permalink: /what-does-it-mean-to-be-truly-educated/
categories: Philosophy
---


On the one hand, a child with a burnt finger knows not to touch fire ever again but on the other, a grown man with so much maturity knows not to avoid the source of his misery despite the constant suffering. Who is more educated here? Or for that matter more intelligent?

The child is already good at scientific deductions in it knows what to infer from its experience but the man seems incapable, what does it say about the man? Is he less educated or is the child more intelligent†? Maybe, maybe not.

Obviously, this is not a fair comparison you say, and I agree. You can’t compare a reflexive response like reacting to fire with the highly complicated dynamics of life you say, and I agree to that too. But on a similar note, should all men be compared using the same scale? Should one’s ability to think be called education? Should one’s ability to learn and acquire knowledge be called education? Or is it one’s ability to extend their learning that should be called education?

What does it even mean to be educated? Is it a systematic process of downloading the technical know-how onto one’s brain? Or is it a process of discovering things through one’s own experiences? Or something else maybe? I think education is a word like consciousness, though one can define it vaguely, any attempt at defining it concretely would mean it is no more the right track for most people.

And there exists a vague definition just like the one we talked about and it states that:

> Education is a process of facilitating [learning](https://en.wikipedia.org/wiki/Learning), or the acquisition of [knowledge](https://en.wikipedia.org/wiki/Knowledge), [skills](https://en.wikipedia.org/wiki/Skill), [values](https://en.wikipedia.org/wiki/Values), [beliefs](https://en.wikipedia.org/wiki/Belief), and [habits](https://en.wikipedia.org/wiki/Habit_(psychology)).
>
><cite>—Wikipedia</cite>

Although the definition looks good at a first glance but it does not address our concern of identifying someone who is truly educated.

In fact, let me correct myself, I think the problem does not stem from the definitions and knowledge that we have about education but from our want to mechanize the process of knowledge(_whatever that means_) acquisition and thinking. An example that I can immediately recall is that of the logic gang’s(Russell, Whitehead, Frege, Hilbert) attempt at mechanizing the process of reasoning, at least from what I gather it didn’t go well for them, and I don’t think it will for us too if we try to create a system around something that mechanizes us. I think it is an organic process, what deserves to be mechanized/automated shall happen eventually on its own but any effort that sprouts from one’s brain with an intent of streamlining other humans will definitely wither away sooner or later for real outcome that is expected of education lies not in obeyance but in autonomy.

True education should help you navigate the right and the wrong with equanimity and not with a restless urge to right the wrong. True education should allow you the autonomy to think for yourself and decide what education means to you and not make you mechanistically follow the beaten path of acquiring technical know-how. Maybe I am wrong here but it is this ability to accept wrongness that is not included in the curriculum, for all through our educational life we are rewarded for our level of rightness and punished for our level of wrongness. May it be in the form of tests, interviews, or jobs. Just to be a little more clear, I am not talking about being comfortable having done the surgery incorrectly, what I am saying is the want to perform the surgery carefully is the organic result that I spoke of earlier but creating a test that tests you for your ability to be right 9 out of 10 times is not organic, for it is trying to mechanize you, and also it is an attempt by the fellow humans to streamline the filtering process that involves humans(Intent!).

One counter-argument that I usually get for this argument is — isn’t redundancy convex, meaning, doesn’t repetition and testing help one get a better hold of the idea. Yes, it does, but how many people do we have right now who are interested in STEM. As much as these processes help, without autonomy, it eventually fails. The trade-off when it comes to human-to-human transactions that are either ill-intended or lazily thought out has historically not been that good. What we need is not a filtering mechanism but a way to let people understand that education means self-sufficiency and help them towards it. What we need is more people working for themselves, and with others with a hunger for creating value and pursuing their creative freedom. What we need is more people knowing that they can do well if they can think for themselves. What we need is more people realizing that wrongs do not necessarily mean wrong†. What we need is more people having the freedom to pursue what they want and not resort to being a slave and follow external command for having to accomplish it. What we need is knowledge as a by-product of creative freedom and not general purpose machines.

> **To be educated should mean creative autonomy, at least that is what I think it should mean.**

In essence, if someone can pickup a letter “i” and call it square root of negative one and add it to number system(imaginary numbers), you should be able to pick “j” and call it square root of zero and add it to number system too, and not worry about the who’s who of the field telling you how you are wrong. Btw they do exist, they are called dual numbers and unfortunately the guy(Clifford) did not chose “j”, he chose “epsilon” and chose to call it nilpotent. But fortunately we still have “j”, use it, think something else😅

Please do share what you think in the comments below 🙏

----------

## Footnotes

1st† precocious kids/prodigal kids.

2nd† Only wrongs that harm others are the definite wrongs (whether directly or indirectly). In essence a symmetric transaction where other humans have same level of control if you harm them.

<br/>
