---
layout: post
title: Action cures fear v I fear action - A false dichotomy?
comments: true
permalink: /action-cures-fear-v-i-fear-action/
categories: Philosophy
---

I have been thinking about this for almost a month now, and I am still not sure if this is even a legitimate comparison. In fact, on one level this may seem like a paradoxical statement, a deadlock of sorts given the circular nature of the argument, but on the other, it feels like there is a clear reason for choosing one over the other. Let me explain.

### Truth and the trilemma

In epistemology, there is an idea/thought experiment called the [Münchhausen trilemma](https://en.wikipedia.org/wiki/M%C3%BCnchhausen_trilemma), which says there are only three possible ways to provide truth in response to further questioning.

1. The _circular argument_, in which the proof of some proposition is supported only by that proposition(self-referentiality)

> **Eg.** Chicken and the Egg problem.

  

2. The _regressive argument_, in which each proof requires a further proof, ad infinitum

> **Eg.** If there is god, who created that god, and the god before, and the god before that god and so on and so forth.

  

3. The _axiomatic argument_, which rests on accepted precepts which are merely asserted rather than defended

> **Eg.** Things which are equal to the same thing are also equal to one another (a = 1, b = 1; then a = b)

And the fun thing about this trilemma is all of them are equally unsatisfying. In fact, all of what we call mathematics, physics, or even religion is also based on a combination of one of these three.

### The deviation

Although the chances of this happening is extremely unlikely, but let’s just imagine for the sake of this argument that tomorrow morning when you get up, all of what we know as physical laws were completely changed(bar your ability to ponder over it), and what we currently know as electromagnetism or relativity didn’t exist at all. But somehow your existence was still possible i.e., you were still alive, would you still believe in science?

The fact is, science was never a belief system in the first place(as some people make it to be, and use it to virtue signal). Science was, is, and always will be a tool to understand the existing abstraction based on the fundamental principles i.e., the axioms — **The third lemma**. And this will be true no matter what the new laws are, as the new school of physics will be built upon the foundational laws then present.

So, what about religion? God — A combination of all the three lemmas; Tenets — Axiomatic argument. Period!

In fact, the truth value of the trilemma itself is based on the trilemma. — Circular Argument.

If any of this seems confusing to you, don’t worry, it does so because it is. This is not to say that nothing is true, but that the provability of any argument rests on a paradoxical idea. What a paradox!

In fact, this is primarily the reason why we have so many paradoxes. But on a brighter side, I think science has been doing just fine, so my point is why not try and extend it to our argument.

### Course Correction

Now that we have some framework to work with, my question is, is it possible to handle the statements such as the one in the title without succumbing to the paradoxical nature of the argument? My guess is, yes. But to go forward we will need an assumption, that is, _the outcome of the action is of some significance to the person who is afraid to perform the action_.

Now with the assumption in place, we can treat it as an axiomatic argument — The third lemma, can’t we?

So, now we have an axiom that can exhort bias towards action despite the side effects of the misaligned emotions; and the misaligned emotion being the fear here. In essence, all it needs is establishing a prior so that you don’t strengthen your [aliefs](https://en.wikipedia.org/wiki/Alief_(mental_state)). True? No?

In [philosophy](https://en.wikipedia.org/wiki/Philosophy) and [psychology](https://en.wikipedia.org/wiki/Psychology), an **alief** is 
an automatic or habitual [belief](https://en.wikipedia.org/wiki/Belief)-like attitude, 
particularly one that is in tension 
with a person's explicit beliefs 

### The Dilemma

Having said that, I do feel that the above approach resolves the paradox, but I also have my doubts. In fact, the argument is not truly overlapping in a sense, but it does have some intersection when you draw the Venn diagram, and this essay deals only with that small overlap where your actions and fear are constantly in a deadlock zone. But even in that case, how much of it is driven by your beliefs? Or for that matter your aliefs? Can you act mindlessly based on a rational idea? Can you bypass your emotional drivers and sustain the side-effects of your misaligned emotions? I do not know. But what I do know is this is a question you will stumble upon when you invert the original question i.e., “Why am I afraid to do this?” into “What would I do if I were not afraid?”

I know this is a subjective matter, and a lot depends on one’s current state of mind over some quirky rational trick to tame the emotions. I understand that one’s aliefs play a very crucial role in this, but to what extent? Is the formation of misaligned emotions due to one’s aliefs? Or are the misaligned emotions themselves a form of aliefs that cannot be weeded out, not matter the efforts to automate? Will we ever know any answer to this?

---

It was a fun topic to think about, and hopefully, I will be diving a little deeper into it in some of my future essays, but for now, this is it. Somehow it feels like a dead-end every time you enter the valley of emotions, it feels like rationality and emotions will never have a meeting point. I do not know why, maybe you do. If you do, as usual please do let enlighten me with your counter-view point(politely) in the comment section below. Until then take care and be happy 🙂


---

## References

[Aliefs. (Wikipedia). ](https://en.wikipedia.org/wiki/Alief_(mental_state))

> In philosophy and psychology, an alief is an automatic or habitual belief-like attitude, particularly one that is in tension with a person's explicit beliefs. For example, a person standing on a transparent balcony may believe that they are safe, but alieve that they are in danger.

[Münchhausen trilemma. (Wikipedia).](https://en.wikipedia.org/wiki/M%C3%BCnchhausen_trilemma)

> In epistemology, the Münchhausen trilemma is a thought experiment used to demonstrate the impossibility of proving any truth, even in the fields of logic and mathematics. If it is asked how any given proposition is known to be true, proof may be provided. Yet that same question can be asked of the proof, and any subsequent proof. 


<br/>