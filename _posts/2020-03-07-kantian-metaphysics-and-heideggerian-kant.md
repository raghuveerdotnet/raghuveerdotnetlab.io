---
layout: post
title: Kantian metaphysics and Heideggerian Kant
comments: true
permalink: /kantian-metaphysics-and-heideggerian-kant/
categories: Philosophy
---


Kant has always had the tendency to use the term Metaphysics in a very restricted sense, that is, in the sense of the doctrine dealing with the supra-sensible realities such as GOD, but how well can you extend it to explain/understand the ideas other than the one within the framework of your definition. This is something that has troubled philosophers and non-philosophers alike who have read the “Critiques”; and we have had numerous interpretations including the one we will be discussing briefly today by Heidegger, but the problem is that almost none of them agree with each other, to the point that reading their accounts will only get you more confused about the original intent of Kant. To simplify things I will only take up the argument by Kant on the impossibility of Metaphysics as Science, and the reason being the utility of this argument in portraying the restriction imposed by Kant on the term “Metaphysics” and “Science”.

Kant has been a central figure in contemporary philosophy and there is no two ways about it, but the problems are the interpretations, whether you are trying to grasp it yourself or trying to understand it through the famous/infamous accounts of his philosophical views by others. For starters, when we talk about science or metaphysics, none of us are talking about the exact same things, are we? for the consensus on the meaning that is arrived upon through a shared definition in a dictionary is not how it’s used in the real life. When he talks about the impossibility of Metaphysics as Science, he means the body of scientific knowledge by the term Science, or more appropriately in Kantian terminology, the knowledge about the synthetic a priori judgements. Is it right though? In fact, this becomes the main point of the Critique. However, this only leads to further complications on his part, meaning, to avoid this confusion between Metaphysics and Science, he makes the distinction of things that can be experienced (in Kantian terms: **_Phenomenon_**), and that which transcends the experience (in Kantian terms: **_Noumenon_**). Following Rousseau, he even holds that man dwells in both the realms. Thus calling metaphysics a natural disposition of the human mind (which I want to agree with, but I just can’t understand it properly to realize the need for the distinction. Is he talking about the epistemic uncertainty? Or is it the temporal difficulty that arises as a result of his transcendental enquiry?).

The philosophical literature on Kant’s attitude towards metaphysics upholds mainly two of its aspects:

1. The tradition of metaphysics to be found in Kant’s critique system.

2. The interpretation of Kant’s criticism itself as a metaphysical system.

Strange loop, huh? It certainly got meta there. And it is here that Heidegger comes in and takes the second approach (probably one of the very few in taking that approach), causing trouble for all of us who are accustomed to metaphysics as merely a non-empirical system of studies. Heidegger in taking note of Kant’s idea of calling metaphysics as a natural disposition of human mind treats his work as a doctrine of being (see: _Being and Time_) I.e., Critique of pure reason as a metaphysics of man — the ontological speculation concerning man’s place in the world.

It is interesting to note that in doing so, Heidegger takes the route of attributing what Kant calls as transcendental about his enquiry of metaphysics to temporality, for what must posses the knowledge of “being” must encompass the times past, present, and future.

And in interpreting Kant, Heidegger not only diminishes the value of understanding by expressing its finitude, he also takes it to the next level by portraying it as inferior to sensibility by giving the examples of Kant’s argument in Transcendental Aesthetics about the priority of intuition, the mode of receiving intuition before thought that unifies the manifold of sensibility. (**In muggle speak:** _We are too dumb to understand everything, so understanding is finite and when we feel like we understood something that is beyond us, it is mostly the supra-sensible nature of metaphysics ie. a transcendental entity capable of breaching the arrow of temporality that is allowing us to grasp without actually understanding it, but deceiving us to actually make us feel like we understood it; and thereby giving us the glimpse of the finitude of human existence._)

From this, we can clearly see that Heidegger’s interpretation surely challenges the traditional reading of the Critique. And it also upholds the basic contention that knowledge or the possibility of experience is based upon understanding, being in terms of temporality, conceived as original yet finite.

If you were to go with the Heideggerian Kant, the traditional ones usually have a comeback that uses the conceptions like freedom(infinite) claiming it arises from the faculty of reason, thereby calling the finitude argument as a misinterpretation (See Cassirer’s response to Kant and the Problem of Metaphysics). In fact, Cassirer famously and fiercely came down at Heidegger’s interpretation by saying that Heidegger misinterpreted Kant and pure reasons can, in fact, be used to understand that which is beyond what is experienceable --- like freedom in this case(I have my doubts here); and that Kant’s use of transcendental imagination was merely for architectonics.

I mean at this point what should a philosophically oriented layman like me who likes to dabble in philosophy and cognitive science do? Go with the traditional notion? Or the Heideggerian Kant? Or create a new brand of Kant that I understand better?

*Endnote:* I have only started reading some Kant recently and I am really fascinated by his works already. I just completed the first Critique and the book by Heidegger on Kant titled “Kant and the Problems on Metaphysics”, so I do not have very good grasp over Kantian philosophy, but there is something here that I feel is buried deep down that is trying to get out. And after reading some of his works I am starting to think that people who dismiss metaphysics easily are those who have not even tried to take a look at it. Anywho please do comment below and let me know what you think of it, and if you would like me to go deep down this rabbit hole.

---

## References

Kant, Immanuel. (1781). *Critiques of Pure Reason.*

Heidegger, Martin. (1929). *Kant and the Problems of Metaphysics.*

Heidegger, Martin. (1927). *Being and Time.*

 Cassirer, Ernst. (1930). *Cassirer's review of Kant and the Problems of Metaphysics.*


<br/>




