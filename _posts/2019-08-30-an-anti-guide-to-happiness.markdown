---
layout: post
title: An anti guide to happiness
comments: true
permalink: /an-anit-guide-to-happiness/
categories: Philosophy
---

I was pondering some days ago as to what makes a good life and what came out of it as a conclusion was surprising – the concept of a good life is just too subjective to be formulated in universal terms. And if you are anything like me who enjoys reading philosophy, it must be difficult to decide between all that wisdom as to which is relevant and which isn’t, what to follow and what not to. Some preach the greatness of classical greco-roman philosophy; some the Hammurabi’s symmetry; and some the teachings of Buddhism. Between all these, what should a commoner like you and me do – I think a good way to go about it would be to create your own guide, or more like an anti-guide that reminds you of things that you don’t want to do because they make your life miserable. One advantage of an anti-guide is, they are usually shorter than guides and are self-evolving in nature. I am evolving and so will my anti-guide, hence the “part I” in the title. Here are the 12 things that I think everyone must avoid to avoid a bad life.

1. Choosing order over chaos when in dilemma.

> For what it matters, chaos is just order disguised to conceal its identity. First to uncover reaps its benefit, the rest talk about the first – Antifragility. I don’t think “fortune favors the brave” is just an adage, try it.

2. Emulating mannerisms of the highly accomplished people because you think that is what makes them a genius.

> In fact, it is exactly what it sounds like, mere mannerism. Correlating wrong factors like mannerism to innate talent, critical thinking ability, courageous attitude, extreme passion, and hard work is like correlating race to IQ.

3. Thinking normative.

> Building long-lasting relationships, helping others, and being compassionate are probably the only normative actions that one should ever pursue, anything else is entrapment. Let the thoughts run wild, sit like an enlightened one and notice them all, if you like something and if it does no harm to others, just do it even if it is not normative.

4. Learning to gain knowledge.

> Most men gain knowledge not when they are in pursuit of it, instead when they endeavor to accomplish a task that gives them the kick. These are the men who go back and write their learnings for others to read. If you wish to read, read for the sake of it, not for gaining knowledge.

5. Mistaking inspiration drawn from elsewhere for real reward.

> Inspiration drawn from elsewhere can be highly motivating(a virtual reward), the real reward lies in keeping your heads down and putting in those hours.

6. Knowing the name of something instead of knowing the thing.

> Understand an idea and name it what you want. Mnemonics are an excellent way to remember the ideas but don’t forget the idea itself in the process. Brag about the idea, not the name, former show clarity and the latter sophistry.

7. Letting the brain preserve the ideas.

> Transient are those ideas that you want your brain to preserve. It only creates and memorizes – rest is to be retrieved from means outside. Write down every new idea that you get and thank your brain later.

8. Chasing clarity to gain courage.

> Mental clarity is the child of courage; not the opposite. Even with all your knowledge on swimming from all the books you’ve read, you are still destined to sink and die when you fall off the boat. Always get in and then understand, never understand first and then get in unless it doesn’t require courage.

9. Creating inflexible notions

> What is not seen is not a proof for its non-existence and what is not seen is definitely not proof for its chances to never be seen again in future. And the same holds true for what is seen. Applies mainly to mindless followers of religion and science in they become intransigent once they see the reasoning behind a line of thought. Today’s understanding accounts only for today’s reasoning, tomorrow is always a new day. Most theories undergo changes over time.

10. Moving away from your passion because it is not future-proof.

> Life is too random to be worrying about someone else’s prediction. Let your predilection be your guiding heuristic. For you may live a comfortable life going with their prediction but it will be uncomfortable to live with it.

11. Venturing into the idolatry of the successful.

> Survivorship bias: Logical error of concentrating on the people or things that made it past the selection process and overlooking those that did not, typically because of their lack of visibility. There are millions of geniuses, we only get to see Elon Musk and Steve Jobs, and then some more who admire them and fail. Gather the courage, jump into the ocean and start swimming. Maybe one day if you survive we can meet.

12. Playing the short game over the boring long one.

> The problem is – the interest rates in compound interest-based schemes are low, so most of us are content with simple interest, only to inveigh about the injustice in the future. Snowball effect: a process that starts from an initial state of small significance and builds upon itself, becoming larger (graver, more serious), and also perhaps potentially dangerous or disastrous (a vicious circle), though it might be beneficial instead (a virtuous circle). Consistent effort always compounds, if you do the right thing it will be a virtuous circle, else a vicious one – choose 8 hours sleep daily over 12 hours on the weekend, choose timely small meals over gorging at once, choose 25 pages a day daily over reading at one go, choose 30 minutes workout daily over lifting like atlas once a week, write a page daily over writing 50 pages over a weekend, code an hour daily over slogging on the weekend, play the piano an hour daily over practicing 4 hours on the weekend and above all choose habits over goals.

This is not comprehensive by any means, and nor is it universal. I wrote this keeping in mind what was plaguing me from avoiding a bad life, hopefully it helps you too. And by the way, it was never about me writing something that could help you, instead it was about you finding out things that you wanted to chip out of your life. Compile your own anti-guide and share it in the comments below or in your blog/twitter page and share the link.

As usual let me know how you liked it(politely), until then be happy. see ya later 🙂

---

## References

Parrish, Shane. *[Farnam Street](https://fs.blog) – [Compounding Learning](https://fs.blog/2019/02/compounding-knowledge/), [Long Game](https://fs.blog/2018/10/long-game/)*

Feynman, Richard. (1978). *[Knowing the names vs Understanding it](https://www.youtube.com/watch?v=lFIYKmos3-s).*

Taleb, Nassim Nicholas. (2012). *Antifragile.*

Taleb, Nassim Nicholas . (2017). *Skin in the Game.*

Plato. (375 B.C.E). *The Republic*

Franklin, Benjamin . (1996). *The Autobiography of Benjamin Franklin*









