---
layout: post
title: Antimimetism - A conflict prevention mechanism
comments: true
permalink: /antimimetism-a-conflict-prevention-mechanism/
categories: Philosophy
---

I do not know if imitation is the highest form of flattery but what I do know is imitation is the best way to learn things. You want to learn to trade? mimic a trader; You want to learn to play football? mimic Pele; You want to play the piano? mimic Chopin. The scope of imitation is not only limited to mimicking people, it also extends to mimicking animals, nature, inanimate objects, and sometimes even the self. It is through imitation that we learn and it is through imitation that we form desires, for all of us are blank slates when we are born, aren’t we? But what we don’t realize is it goes beyond learning and desires, it forms the core of all our conflicts.  

**René** **Girard,** one of the most prominent philosopher cum literary critic of the 20th/21st-century calls this the “Mimetic Theory”. He says that all desires that we have are directly or indirectly mimetic in nature. Furthermore, he goes on to claim that if there is a normal order in societies, it must be the fruit of an anterior crisis, which in turn must be due to mimetic desires. And we can see this very clearly in our societies, both past, and present. All the major wars in the past have been due to kings wanting to rule the same piece of land and all the major economic booms/crashes in recent times have been due to people following the same path—the ongoing startup frenzy, the dotcom boom/crash, the 2008 subprime mortgage crisis, etc.  

It doesn’t have to be at a bigger scale, one can see the effects of mimetic desires even in one’s day-to-day life. For eg. Would you want money or fame or power if these things were never wanted by anyone else? Ponder over it. Even if there was a first-person, who placed any value to it, he/she must have derived that value from some source, whether it was from themselves or nature or a group. My notion of desires is that it is largely non-emergent in nature, meaning, even if we were to accept that one could form his/her own desires, in the end, it transforms and converges with the larger-self when the interaction with the larger-self grows. This larger-self could be anything, from your spouse, the community you live in or the larger society that is the world. When your ideas and desires align with that of a larger-self, it means that over a long enough period with fewer resources and more people with similar desires you will have to compete for it. Thus creating conflicts.  

The central idea of this essay is that conflicts can be avoided and that too easily. The problem with most people is that they can’t let go of their interests and ambitions, which I believe is the most important skill that people must possess. This is not to say that one mustn’t strive towards achieving their goals and dreams, all I am saying is if the area of interest is overcrowded one must be able to think probabilistically as to what their odds are and act accordingly. Overcrowding is always a sign of an impending conflict. And in such scenarios, there will always be someone who will be willing to sacrifice more than you. If you are in a business and you think of gaining more market share by lowering your margins, there will be someone who will endure even lower margins and that too for a longer period of time, what do you do? In this rat race of coming first in the same set of dreams and desires, there will always be someone who will obviously have more endurance, more intelligence, and more grit than you. I don’t mean to discourage anyone but I think competition is overrated. There are only 7500 undergraduate seats in Stanford, and do you know how many apply each year? ~100K. Each year Y-combinator—the leading startup-incubator cum seed-funding company—gets around 2K-3K applications and how many do they take in? ~50-60. If you are going to feel bad about yourself for not getting into these institutions or not being able to be the best in what you do? You are not thinking hard enough. This is what I meant by being able to let go of your desires and ambitions. Every time you feel this way ask yourself this one question: How did the desire originate? More often than not the answer to that question will lead you to a chain of events that culminated in the formation of that desire. It is much easier to detach oneself from something that has many reasons than just one reason(which usually is more along the lines of – this is my passion).  

Fractalize yourself, meaning, split yourself into many entities such that one entity not succeeding in one thing doesn’t affect the other entities. Now the problem is your entire ego is centered around a single idea of what defines you. The more you fractalize yourself, the more you can let go of things. Take an example of reading. Let us say you are reading a boring book, most people co-relate reading a boring book to the act of reading itself being boring, which has made many of us quit reading as a result of it. On the other hand, if you fractalized yourself into multiple entities, you can categorize that particular book to be boring while retaining other parts of yourself that can still engage with a good book. Whenever you feel bad about something think of yourself in terms of mental patterns where you are not defined by that thought but by various thoughts at various moments hence making you a fractalized object, meaning, the bad thought now is only one of various entities/mental patterns that you are. So if someone insults you, they have insulted the mental pattern that takes in the words as insults and when you have desires, the desire does not define you. This way you can easily let go of your ambitions and desires because they are not your desires but desires of a part of an infinite entity that you are.

Finally, when you are able to do that, you will have arrived at a stage where you will need to pursue something else. This is where our idea of anti-mimetism comes into play. Anti-mimetism, as the name suggests, is the idea of not mimicking something when you realize that the probability of conflict is high. If there are too many people, or something is trending, or something is touted as revolutionary—it is a sign of an impending conflict. Now the question is what do you do? If mimetic desires are inevitable, you will have to mimic something, but this time you pick a different desire in your circle of competence i.e., things that you know well. For example, if machine learning is the most sort after specialization, and you happen to be a software engineer, go do theoretical computer science or graphics programming. Similarly, if cardiology is the most sort after specialization in medicine, go do endocrinology or whatever is least preferred. I am not an expert at medicine but what I do know if you have a circle of competence in something and you know it then you always can switch your desire by fractalizing yourself and applying anti-mimetism. This has worked for me and for many others I know of. Try it and if doesn’t work you can always revert to your ways of doing things.


----------


It was fun as usual. This one may be a little more because of having to deal with unconventional ideas. Anyhow I enjoyed writing, hope you did reading it too. As usual please do enlighten me with a counter viewpoint(politely). Until then be happy and see ya later 🙂 .

----------

## References

Deliri, Ase, Dellena, Luca, Norman, Joseph. (2019). *Risky Conversation Podcast.*

Taleb, Nassim Nicholas. (2012). *Antigragile.*

[Girard, Rene](https://en.wikipedia.org/wiki/Ren%C3%A9_Girard#Girard's_thought). *Mimetic Theory.*



<br/>
