---
layout: post
title: For the new decade
comments: true
permalink: /for-the-new-decade/
categories: Philosophy
---

> “I say nothing works anymore, but I get up and it’s tomorrow”<br/>
<cite>---James Richardson ,Vectors</cite>

And it is tomorrow. Everything is fine, I feel much better, I see a ray of hope. There are emotions, there is joy, but there is also in my mind lurking somewhere the impressions of past, the imaginations of future, nothing’s changed it reminded, but I got up, and it was tomorrow.

> “By convention sweet is sweet, bitter is bitter, hot is hot, cold is cold, color is color; but in truth there are only atoms and the void.”<br/>
<cite>---Democritus (trans. Durant 1939 )</cite>

I searched the atoms, I searched the void, but there was nothing at all. I felt the heat, and then the cold, and now I know the truth.

> “Mr. Wonka: “Don’t forget what happened to the man who suddenly got everything he wanted.”  
> Charlie Bucket: “What happened?”  
> Mr. Wonka: “He lived happily ever after.”<br/>
<cite>---Roald Dahl, Charlie and the Chocolate Factory</cite>

He did live happily ever after.

### What lies ahead?

I really do not know, maybe I’ll get up and it will be tomorrow. I have said it many a time in my other essays that the only thing that I find solace in besides reading, is writing prose, and it hasn’t changed in the last one year, and I hope it doesn’t change for years to come. In fact, one of my wishes for the next decade is to write an essay, however bad, every day for the next ten years without fail. Not all of them public though, some personal, many public.

As for everything else, I want to experience things sweet and bitter, hot and cold, good and bad; and maybe on the way stumble upon the truth that Democritus was talking about? For I am of the belief that it is only knowledge that can be passed on, while wisdom requires that each one of us acquire it on our own, through our experiences, and the knowledge that was passed on. So however profound and intelligent you are, the truth demands that you experience it yourself.

Finally, I wish to be happy, not just when I get things I wanted, even when I don’t get them. I want to learn to be comfortable being uncomfortable. Have a reasonable health, a calm mind, lots of love, and maybe learn a thing or a two in the process. That is all.

As usual share your thoughts in the comment section below and let me know what are your wishes for the next decade. 🙂

----------

The quotes you see above are some of my favorite quotes that I sometimes agree with and sometimes disagree, and that is what you see when you read the paragraph below each one of them.

<br/>
