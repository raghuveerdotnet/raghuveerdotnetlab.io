---
layout: post
title: An era of converging minds
comments: true
permalink: /an-era-of-converging-minds/
categories: Philosophy
---

Of all the unanswered questions since the beginning of humanity, perhaps the most confounding one is: what makes an answer worthy?

Is it not true that all of us have answers to all our questions in some form or the other? The meaning of life is different for each one of us, for some, it is the struggle and for others there is none; To some, a successful life is that of unparalleled wealth and others, to survive the day.

It is not easy to have an answer that is universal – an answer that can really be an answer for all of us. This makes me think as to what creates this difference, why can’t all of us converge all the time on the same things? Why is it that some answers resonate and some don’t? Who is to say what sounds right to the world should sound so for the rest of us too?

It is absolutely necessary to understand that the rebellion to find one’s own answer is a never ending one. And you are the judge of the answer’s worthiness. But wait, are you though? I am going to wager a no here, at least from the looks of it, you merely seem like a consumer of an answer that secures the highest rank in your immediate observable world. This is the premise – we are converging.

If a dozen years ago someone foretold you that you would have all the answers to your questions, or at least the majority of them contained in a box, you would have thought of them as a lunatic. What would you call them now? An oracle? A prophet? The truth is we all are part of this lunacy now and that is all that matters. And it doesn’t take much to realize that the immediate observable world that I spoke of earlier is only ever expanding. In fact, even with all the echo chambers that you have created for yourself, you are still one helpless being.

Of course, this convergence was bound to happen. It would have happened even if the box had never been invented because you have always been concerned only with your observable world. And the rest of us have always wanted to become a part of that world. This is already starting to make sense, isn’t it? We have always wanted this to be your answer and the other side that wants to convince you against our answer is either outnumbered or poor at communication skills.

When I think of this, one example that stands out is that of privacy. The other side has always tried to induce some amount of panic, but how long does it last? Even here, your immediate observable world assures you that it is a one-off issue. You are then made to go back to your relaxed notion of privacy. The only counter one can come up with is that of financial transactions, where you still value your privacy, even there the caution shown isn’t due to the other side, instead due to the convergence on panic as a measure to keep it running.

To suggest that you don’t get to decide if an answer is worthy of the question, may make it seem like you don’t have any role to play in it but it is to be remembered that you too are part of my observable world. This gravitational pull is only going to increase and beyond our imaginations. I don’t know if it is for our good but what I do know is whenever we get it, the answer is going to be unanimous. The convergence is for real.

It will be seen that this is something we will all agree. Maybe not now, but when the prophecy is fulfilled the minds will not diverge. Maybe the lunatics were always right but who is to say? Relax, and let the lunatic in you diverge. Stand out and not above. Break the formation of an era. This is your only hope to end it.
