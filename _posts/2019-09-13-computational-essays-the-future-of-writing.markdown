---
layout: post
title: Computational Essays - The future of Writing
comments: true
permalink: /computational-essays-the-future-of-writing/
categories: Writing
---

The one thing that I find solace in, besides reading, is producing prose, however bad it may be. The joy of articulating your thoughts in writing is indescribable. And If I were to guess, this must be the case for anyone who writes. People like to write, whether in the form of maintaining a personal journal, writing essays for blogs, or writing a book. No matter, how good a speaker one is, the power of articulating one’s thoughts through a written medium is just unmatched. I think this has to do with our ability to read, the inherent non-linear nature[1](https://raghuveer.net/2019/09/13/computational-essays-the-future-of-writing/#footnote) of reading gives it the edge over having to listen to something in a linear fashion to make sense of it.

People have been writing prose for ages now, sometimes they just use words — sometimes they use visual elements like images — but in the modern times with so much computational power at our fingertips, I think we are at a juncture where we can articulate even better with much more precision and completely transform the way we convey information and our thoughts to the world with the help of what is called as Computational Essays.

**Stephen Wolfram**, a physicist and the creator of the wolfram computational engine roughly describes computational essays as a way to convey relevant information as a function of computational thinking. In one of his blog post titled “[What is a Computational Essay?](https://blog.stephenwolfram.com/2017/11/what-is-a-computational-essay/)“, he makes a compelling argument for the various use cases of Computational Essays to improve productivity and comprehension. It is just mind-blowing to see the applicability of computational thinking in the domains that are not conspicuously data-reliant in nature. The prevalence of computational thinking is already well established in fields like physics, computer science, economics etc. But the idea of being able to extend it to other fields is really exciting and mainly to train kids to think computationally so that they can concentrate their efforts on non-trivial things and leave the trivial computations to the machines.

The idea is when we start writing computational prose using tools such as Wolfram Language, our brains can jump directly to what we call second-order thinking and bring the currently counter-intuitive and non-explainable ideas to the intuitive realm.

Wolfram Language developed by Stephen Wolfram is probably the best tool available in the market right now to develop computational essays and it does not come cheap. I am of the belief that what is not democratized can never be the future and I say that despite being a hardcore user of the Wolfram Language. Having said that, I don’t think we need Wolfram Language per se to get started with this new kind of writing. Anyone with a computer capable of running python, which is pretty much every computer that is in existence today, is capable of producing extraordinary computational essays.

Before we go any further, let us first look at what is meant by a computational essay. Suppose you have to write an essay for your English literature class on the works of William Shakespeare using Hamlet as a case study. What do you do? If you take the regular route, the only way is to probably google around a bunch of papers and articles on Shakespeare, maybe read “Hamlet” completely, and make notes out of it based on your understanding. But let us try to look at it from a different perspective. The computational thinking perspective:

Although I do think that it is a far-fetched example but this makes for a good example to showcase the applicability of computational essays even in the not so obvious domains.

**For ex.** In our case, we can formulate a set of generic question on the author’s style and then try to extrapolate based on the computations. How does the author go about introducing his lead character? Does he build up the character first or introduces right away?

Fetch text from Hamlet, Get the positions(in character range) where the author uses the name of the lead character i.e., Hamlet and print the relevant lines to see how he introduces the character.

![](https://knowledgecontinuum.files.wordpress.com/2019/09/firstoccurence-1.jpg?w=837)

Here we can infer that Shakespeare has a tendency to introduce all his characters early on in the book through a character outline, this could be more useful in the case of authors who use the story to introduce the characters. Although we do need to read the book to know that it is written in the form of a play, nonetheless such computations can provide us some great insights that others going linearly would not be able to think about. With little machine learning, we could have also analysed all the lines involving Hamlet to understand the mood when the lead character is involved. But let us keep that aside for some other time and try and take this one step further.

Plot a smooth histogram to know the character distribution across the story and know when the author intends to use the character most in the story line.

![](https://knowledgecontinuum.files.wordpress.com/2019/09/hamletoccurence-1.jpg?w=555)

Again, it can be seen that Shakespeare has a tendency to dial-up the engagement of his lead character around the latter half of the plot. This can be **verified** by performing **similar experiments** on some of his other books too. We can use such **quantitative data** to gain more **insights into the style of the author**. To step it up a little, we can even go on to look at the cross-over of two characters, let us say of Hamlet and Ophelia in the same context by plotting a histogram.

Plot a smooth histogram to know the occurrence of Hamlet and Ophelia in the same context in the story.

![](https://knowledgecontinuum.files.wordpress.com/2019/09/capture.jpg?w=672)

The advantages of thinking computationally and writing computationally do not just end there. In my opinion, with the rise of machine learning and artificial intelligence, we can write better thought out prose. And for what it matters it might become a norm to include graphs and data in English Literature studies too.

Well, before you accuse me of corrupting the essence of language studies with computations and graphs, let me tell you that it is not my intention to inject parasites of computational thinking into the field of beauty and elegance. My goal with this essay is to convince you that there is potential in thinking computationally, and it will help us extend the boundaries of our limited mental capabilities. The example demonstrated above is definitely a stretch by today’s standards but its use cases in fields like physics, economics, mathematics, and computer science are already visible.

I mean you definitely can’t write a novel this way using the resources that are available today but you can write a thesis for your school project easily by making use of such computational tools. Same goes for writing essays that need explanations that words can’t articulate.

As for using a tool such as Wolfram Language, there is a wide range of open source tools available on the internet to accomplish the same thing without having to pay for it. Few that I can immediately recall include – SageMath, Maxima, Mathics etc. I for one have been a user of SageMath for a long time now. I switched to Wolfram Language recently to get a feel for it, it definitely is better implemented due to its proprietary nature but all the other aforementioned software provide almost all the functionality that Wolfram Language provides, maybe just a little bit crude.

I think anyone interested in pursuing writing seriously must also have the ability to think computationally in their repertoire. Whether one likes it or not this is going to become a necessary skill in the future as we discover more new things in the computational domains. We can already see that with the advent of computational departments in the universities world over in the form of computational neuroscience, computational finance, computational photography etc.

If not you, at least teach your kids to think computationally and exhort them to tinker with such tools to produce computational essays. It is these kids that are going to be our future, and there is absolutely no need for them to go through the exact same curriculum that you went through when they can get a head start in their ability to see the world via computational thinking.

This one was a little different but I have been meaning to write this for a long time now. I had great fun writing this, hope you enjoyed it too. Please do provide your opinion in the comments below(politely). Until then be happy and see ya later 🙂

---

## Footnotes

† The ability of our brain to comprehend large blocks of text with without having to read word by word, line by line unlike audio.

<br/>