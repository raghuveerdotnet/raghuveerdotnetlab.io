---
layout: post
title: Going beyond the flatlands of rationality
comments: true
permalink: /going-beyond-the-flatlands-of-rationality/
categories: Philosophy
---

<img src="/assets/img/rationality.png"/>

The thought of flatland brings almost automatically the thought of Edwin Abbott, and for two very good reasons. To begin with, it was the first of its kind i.e. a mathematical fiction. Secondly, and more importantly, Abbott was one of the first few writers who helped the rational man see the limitations of his rationality by delivering it to him in his own words. It is remarkable, and in fact, an almost unique attribute of this book to make one realize the presence of the bridles and blinkers that one could never have easily realized on their own. In some ways, I think we still are under that trap in we tend to sublet our faculty of thought to entities that can conveniently use it to do their unimportant thinking while we render unto them the trust that they were never capable of utilizing. I don’t think it is wrong to do so but what I believe is these entities are not the spheres that will show us the third dimension, or that there is an upward at all; For that, we may have to take control of the faculty and think on our own.

I am not proposing a rebellious attitude here, all I am saying is probably reaching dimensions beyond what we can currently perceive might be well within our reach if only we could think for ourselves. Maybe, just maybe what if the things that these entities have to offer is not a bed of roses after all, it is a cesspool full of barbed wires that are waiting only to be discovered when we take control of our faculty back at the end of the lease period. What if what we see as a line is a square after all? and what we see as a giant sheet of paper is a sphere waiting to show us the upward? I think it is within us that the sphere lies and not outside. And I also think there is no entity out there that can do it for us, no matter how low a margin we set for its success, it will always fall short of our expectations at the end, partly because it can’t rationalize the unknowable and partly because it won’t. Smoke may not necessarily mean fire, it may mean a myriad of different things. It is this epistemic humility that I am talking about that can help us transcend the dimensions that we can currently perceive. What you choose may not be the right track, for you can never know the opportunity cost in true sense. So, why not just take the plunge and discover what life has to offer. Isn’t it that one can connect the dots only looking backward? Isn’t it that only in retrospect can one know if it was the right thing for them and not apriori?

<img src="/assets/img/twitter-post.png"/>

> What is not seen is not proof for its non-existence and what is not seen is definitely not proof for its chances to never be seen again in the future. And the same holds true for what is seen.
> 
>---Anonymous

---

Do let me know what do you think in the comments below 🙂

<br/>