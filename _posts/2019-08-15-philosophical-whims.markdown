---
layout: post
title: Philosophical Whims
comments: true
permalink: /philosophical-whims/
categories: Philosophy
---

As you read more and more philosophical books, and books on timeless wisdom, there comes a point when you question the usefulness of this so-called wisdom. At times I have even had thoughts about how I might as well have been using philosophy as a crutch to justify my inaction and temporary satisfaction, but I am of the opinion that it is inevitable to live without philosophy, for it gives you the strength to pull through this rough terrain called life; and the most beautiful thing about it is—it comes from places unexpected. For me personally, it took someone very close to me to have a serious ailment to realize the fragility of life.

It is only when you are down and out, you realize that you may have a thousand friends but when it is time for you to bear the pain, they can’t bear it for you. Not because they won’t but simply because they can’t. This is where philosophy comes in. It is so beautiful that it can even make someone who is seemingly a feeble minded person look like a warrior when he/she braves through the difficulty. My view is even centuries hence when the world is completely technologized to the last grain of a sand, there will still be debates on what it feels like to be a human and what does consciousness mean. But despite all that, I think we will still have our moments of what I call philosophical helplessness, where you doubt its credibility to help you and think if you could’ve, in fact, done better without it.

As an academic discipline, I have no idea what philosophy is but what I do know is, for the countless dabblers like me it is an ointment to our wounds. It provides advice when no one else is ready to. It prepares you for a battle that no one—even if they wanted—could fight for you. It stands right by you helping you fool yourself just so that you could feel safe and sleep safe. Despite all that, you have your moments of doubts, don’t you?

So long as it is there, it will help countless see meaning in their lives but I must repeat, so long as it is there, it will also exhort the remaining to question why it is there in the first place. I definitely can’t imagine a life without philosophy but I also can’t imagine what would it have looked like without it in the first place, so I can’t speak about that, can I?

Now that I have spoken in length about god knows what, I think I should just end it here. The intention was to write a long form essay on life and philosophy but never thought it would turn out into this hotchpotch As usual please do let me know what your opinions are regarding philosophy in general. Stay happy and see ya until next 🙂

----------

**P.S**. Oh and by the way, the reason behind me writing this farrago of an essay is the apparently profound thought I had as a result of having read _“A treatise of human nature”_ recently. Do check it out. In fact, pick anything by Hume and you will fall in love with philosophy. For the motivated, this is the one that introduces Hume’s guillotine/fact-value gap, which I think is very fascinating when you read the book. I first encountered the Is-ought problem when someone tweeted about it couple years ago and suggested this book, and I’ve been wanting to read this book ever since. Finally, it happened now. Definitely recommended.

<br/>