---
layout: post
title: Branching effects of higher order thinking
comments: true
permalink: /branching-effects-of-higher-order-thinking/
categories: Philosophy
---
Most of the time, things that you see and think are probably not what they are. And especially, complex systems like decision-making can be quite deceiving in that they can disguise themselves as one easy problem when in fact they might have consequences unfathomable to human minds. This is where the concept of higher-order thinking comes into play and this is not a new discovery – it has existed for some time now. Many education systems even incorporate these as part of their curriculum to foster critical thinking. If not the exact term, many of you would have at least heard of the term first-order thinking and second-order thinking. The degree of the order depends upon branching of the consequences, meaning, in case of a first order thinking or intuitive thinking problem, you have some data presented to you and all you have to do is just consume it and be able to see the most obvious outcome. Easy isn’t it? But it is after this point that it starts getting complicated – let’s say you are given insufficient information on something, and you don’t have anything to predict the future but your brain; and now you have to extrapolate the information and make an informed decision considering possible second-level consequences – this is what we call second-order thinking.

For starters, most of us even fail at first-order thinking or more commonly known as intuitive thinking, when the questions are tricky. Let us take this question for example: _A scooter and a bicycle cost $110 in total. The scooter costs $100 more than the bicycle. How much does the bicycle cost?_

If you said $10, think again. This might be the place where your intuition or the first-order thinking fails you. If the bicycle costed 10 dollars, the scooter would cost 100 dollars more than that i.e, $110, which would add up to $120 in total. But the question clearly says that the total should be $110. The right answer is the bicycle costs $105 and the scooter costs $5 dollars.

Don’t fret yourself over this, this was indeed a tricky question. In fact, more 80% of Harvard Graduates gave the wrong answer. (`Thinking Fast and Slow by Daniel Kahneman`)

Now imagine, you are a CEO of a startup during the dot-com boom era. The bubble is bursting, and like every other company, even your company’s stock price is on the fall. what do you do?

> Do you sell your company to some multi-billion dollar company? And what after that? What happens to the employees? What about the future of the company? Do you persist with it and believe in it? In that case how will you pay your employees? What if the plans do not work and the product isn’t good enough to keep the company afloat?
> 
> ---Second/Third Order Thinking Situations.

Now what I want you do is, do the same with your life decisions – the trajectory it could have taken if you took a different decision at each point. And analyse each trajectory at every transition point and see where it can go from there and so on. What I’ve given you right now is the recipe for descending into madness. You will go mad before you figure out all the transition points in your current life let alone all the transition points possible and all the consequences from each of those transition points. This is what I call the hyperlink effect(one that happens to you when click on a Wikipedia link and it keeps pulling you into a whirlwind of links).

Although higher-order thinking is good, the moment you go beyond first or second order, the consequences tend to branch pretty quickly which can be difficult and in some cases impossible to follow up with. What should one do in such a case?

The branching can grow exponentially, also if a situation is new and one has no experience with such a situation, it can be overwhelming and can lead to mental fatigue. And this kind of mental fatigue can make one take the less strenuous route which can be more harmful when compared to a decision made when the person was fatigue-free and could have made a decision based on intuition. This is not to say that decision made on intuition are efficient, they can be in many ways both counter-intuitively dangerous and poor. So, what is a good solution to this?

Honestly, I don’t have a good answer. As per my current understanding, I would recommend that one stick to second-order. To live a normal, happy, and healthy you don’t need a chart of thinking taxonomy. Just go about it one step at a time, be wary of repeating your mistakes again, employ Socratic questioning(the why, and the whats that you can think of) tacitly as part of your thinking process and you should be good. If that feels like a lot of work to you, be a stoic and accept the life for what it is.

I am probably a mix of the Socratic questioning type and the stoic type in while taking a decision I question the intent and questions; and if the result turn out be negative, I turn stoic. I guess it is easier to be stoic when you encounter failure (stolen from _The Bed of Procrustes_).

As usual, please do enlighten me with a counter viewpoint(politely). Until then, be happy and see ya later 🙂
