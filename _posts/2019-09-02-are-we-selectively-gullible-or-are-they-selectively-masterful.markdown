---
layout: post
title: Are we selectively gullible, or Are they selectively masterful?
comments: true
permalink: /are-we-selectively-gullible-or-are-they-selectively-masterful/
categories: Practical
---
Why do we switch to new items when the old ones are just fine? And why do we do it only in some situations but not in others? You don’t like to eat with the same fork if one of the prongs is slightly misaligned; you buy a new toothbrush if you see few bristles misaligned but you keep using a broken bar of soap nonetheless. What makes one distinguish between a bar of soap and a toothbrush?

If we were to believe the information online, most say that such decisions are part of company’s strategy to increase consumption and thereby the profit, why wouldn’t a soap bar producing company want a part in it? This makes me think that most of these explanations are nothing but nutty hogwash. I am not entirely sure though.

This raises a question regarding gullibility, even if we for a moment accept that people are gullible and fall for such masterful marketing strategies, how is it that some pass and some fail?

One theory that I have is even if these so-called companies stop their masterful strategy-making process this cycle will continue. And the reason being incorrect analysis, meaning, these nonsensical strategy delusions that we are fed is, in fact, a systemic process. This explains at least many things – toothbrush companies are successful because the system allows for such provision whereas soap bar companies can neither reduce the efficacy of the soap when it breaks apart nor can they make it uncomfortable or ugly when it breaks.

And by systemic process, I don’t mean the future is deterministic and we don’t have free will to make those strategies kind, although that is true. What I mean is, there is a limit to what you can take credit for. There is no denying that these companies are indeed good at taking advantage when the system allows for it but are not capable of actually re-inventing the system itself. And also, there is no reason to believe that nudging doesn’t work, it does. But my concern is when people study this stuff and extrapolate the results into domains that are totally irrelevant. My real issue with it is when these so-called wizards explain you the idea behind why Kellogg’s carton has an almond-honey flavor instead of only honey. As if it was some sort of epiphany, and the rest of us are too stupid to never have that profound realization.

If you remember a while ago, this term called “planned obsolescence” was going viral due to the smartphone industry. I remember reading almost everyday a column on how the Apples and the Samsungs were gaming us by producing incrementally better phones and luring us into buying them for those gimmicky features. What happened to it now? Why won’t I buy a 1300$ phone every year?

According to this [verge article](https://www.theverge.com/2019/5/17/18629003/us-phone-upgrades-apple-samsung-market), an average American is not upgrading as frequently as he used to. And we can already see even in our own lives how the upgradation cycles have reduced to 3 years from what was a new phone every other year. Can Apple or Samsung do anything about it, Nah! Their profit margin would dip if they did. The AMOLED-screens that all these manufacturers want alone costs 80$, now add the cost of AI-enabled soc; a 4g/5g modem; memory(ram, flash); the sensors(ambient, infrared, gyro, proximity, accelerometer); camera modules(lenses, image processor); testing costs(EDA), etc. Oh and what about the sales and marketing cost. Phew!

  
All these technological advancements and the increasing price at the expense of what? Losing customers? The problem is it was never in their control i.e., for each component is manufactured by a different manufacturer and as these technologies keep getting better, the previous-gen becomes cheaper and some unknown Chinese brand will always take advantage of the system at that point.

Making an excellent product is totally different from being able to make what you want to make. In the former, you start with some ambitious idea but end up with something that you have to make piece with because you spent almost 10 years of your life in it. Some turn out just exceedingly marketable on the way, while some perish. Similarily, marketing many things is entirely different from marketing anything, here the strategies only allow you to take advantage of what is allowed and the result makes you delusional.

Anyway, it was fun thinking about, hope you enjoyed it too. This isn’t a concrete theory in any way, it is just what I felt when I kept hearing all these marketing wizards talk about strategies. I am a huge fan of Rory Sutherland myself, so I have nothing against marketing or advertising. Just wanted to put it out there.

  
As usual please do enlighten me with a counter viewpoint(politely) and until then have fun. see ya later 🙂
