---
layout: post
title: Language, Media, and Libel Laws
comments: true
permalink: /language-media-and-libel-laws/
categories: Philosophy
---

Defamation suit, a term that can send chills down your spine if it is filed against you. You can never estimate the price for your mistake, or for that matter if it is a mistake at all until you are sued. As much as you would like to think you have the freedom of speech, they like to think they have the right to sue. To resolve this confusion, we have what is called the libel laws – laws that should protect genuine victims from a false accusation that can be damaging to their reputation. This made me think as to where do you draw the line? What comes outside the ironical boundary that is the freedom of speech? What justifies the umbrage taken or the one intended?

Some are too philosophical whereas some too political, I doubt we will ever be able to answer these questions in a concrete sense. But what bothers me today is not the jurisdiction of these laws or the boundaries in freedom of speech. It is the issue of irrevocable damage that these laws fail at handling. And the effectiveness of these laws at undoing the harm done by the perpetrator, and the medium that he/she uses.

Furthermore, we can see the two entities that are common in all these issues – Language, and Media. Somehow, the latter has all the power in the world to control the former through a narrative that fits its interests. This need not be your conventional mainstream television news channel – it can anything from a Facebook post to a Twitter hashtag; a popular blog post to a really viral YouTube video.

Consider this, you are billionaire businessman **_X_** and you have a social presence equivalent to that of Tony Stark(Ironman?) – eccentric, creative, and courageous. You run a company **_C_** that designs and develops home automation systems and robotic personal assistants.

One day, a reporter knocks at your door and asks you if it is true? You are baffled. You shut the door without answering him and immediately rush to the TV in your living room, and tune in to the news channel. What you see shocks you.

> Is Mr. X really the eccentric genius that we wanted to believe or a twisted creep who wants to watch you?

> The early signs were always visible, says one of his former partners. Mr. X cut me off for not being complicit.

> He gifted me one of the most expensive cutting-edge robots for my birthday, I am scared, says one of the female executives.

What do you do? You call your lawyers and want to sue everyone involved because you know someone is framing you but who? The problem is you have already lost the war, now it doesn’t matter if you win the battle of the defamation suit. A billionaire’s lawsuit victory never reaches the masses because you apparently influenced it with your money but your wrong-doings do. The cost was borne not just by you, everything that you ventured in has encountered irrevocable damage.

Would I buy your product? Nope, not in another 10 lifetimes. I would be so wary of your company’s name that I would be apprehensive to talk or react if I saw one a 100 meters away. This is the state of libel laws, ineffective.

What was at play here was not so much the malicious intent that all of us want to believe but the rhetoric by the media. Isn’t it amazing that a 25-year-old sitting at a news channel office can turn the phrase “_eccentric genius_” to “_twisted creep_“? or what was the “_result of_ _financial fraudulence_” to “_early signs_ _of perversion_“. How do you right the wrong? Who is to say that the media was at fault, isn’t it their duty to report the news in a fashion that catches the attention of the viewers?

Language is twisted, and so are many people using it. What is missing here is the rigor. And if we closely look, there are only two fields that handle rigor well – first being the sciences (hard ones), and the second being law. Isn’t that the reason you won the lawsuit? If we are going to have a society where we want to increase the efficacy of law, we need to enforce rigor in the mediums that do not require creativity, and I think that news channels and the political mediums are certainly part of it.

Anyway I rest my case here, and before leaving If anyone is interested in pursuing this thought and taking it to another level, do look at this concept called _Emotive Conjugation_ in linguistics also called _Russell Conjugation_ popularized by the famous mathematician and philosopher _Bertrand Russell_. It is similar to what we discussed in its effect but mostly deals with changing the word to suit your preferred connotation, positive or negative. One example would be calling someone “_firm_” vs “_obstinate_” Similarly, another such example would be that of “_I am righteously indignant_” vs “_you are making a fuss over nothing_” . It is used all the time by the media to promulgate a certain narrative.

Once again, it was really fun as usual but a little serious this time around. Hope you enjoyed reading it too. This essay is actually inspired by an essay that I was fortunate to stumble upon – an editorial piece called “**_As I Please_**” by the great _George Orwell_ in the 1956 edition of _The Tribune_. The best place to get it is probably the book called **_“In front of your nose”_**, collection of essays by _George Orwell_.

And as usual, please do enlighten me with a counter viewpoint(politely). Until then be happy and see ya later 🙂
