---
layout: post
title: Resources
comments: false
permalink: /resources/
---

## Found in the wild (Source: Twitter, Reddit, HN)

### Resources: Github Repos, Blogs, Websites, Youtube Videos, Channels

----------

BLOGS and WEBSITES

— [Lesswrong](https://www.google.com/url?q=https://www.lesswrong.com/&sa=D&ust=1583943622543000) (Start with this sequence called "A human's guide to words")

—  [Gwern](https://www.google.com/url?q=https://www.gwern.net/&sa=D&ust=1583943622544000) (essays and independent research)

— [SlateStarCodex](https://www.google.com/url?q=http://slatestarcodex.com&sa=D&ust=1583943622544000) (psychology blog)

— [Codex Atlanticus](https://www.google.com/url?q=http://codex-atlanticus.it&sa=D&ust=1583943622545000) (Da Vinci's Real Works made interactive)

— [Internet Archive](https://www.google.com/url?q=http://archive.org&sa=D&ust=1583943622545000) (If it is not there, it is probably here)

— [Jestor](https://www.google.com/url?q=https://www.jstor.org/&sa=D&ust=1583943622546000), [ZLibrary](https://www.google.com/url?q=https://b-ok.cc/&sa=D&ust=1583943622546000), [Libgen](https://www.google.com/url?q=http://libgen.is&sa=D&ust=1583943622547000), [Sci-hub](https://www.google.com/url?q=https://sci-hub.tw/&sa=D&ust=1583943622547000) (The free ones)

— [Stanford Philosophy](https://www.google.com/url?q=http://plato.stanford.edu&sa=D&ust=1583943622548000) (Everything philosophy)

— [ScientificCommunicationAsSequentialArt](https://www.google.com/url?q=http://worrydream.com/ScientificCommunicationAsSequentialArt&sa=D&ust=1583943622548000) (A different take on scientific communication)

— [Computational Trinitarianism](https://www.google.com/url?q=http://ncatlab.org/nlab/show/computational%2Btrinitarianism&sa=D&ust=1583943622549000) (Computational Trinity as an idea to unify different fields of computation)

— [Five books](https://www.google.com/url?q=http://fivebooks.com&sa=D&ust=1583943622550000) (Get five top books on everything)

— [Computational/Design Essays](https://www.google.com/url?q=http://worrydream.com/refs&sa=D&ust=1583943622550000) (Great resource with excellent ratio of value to production on web)

— [A visual enigma emulator](https://www.google.com/url?q=https://observablehq.com/@tmcw/enigma-machine&sa=D&ust=1583943622551000)  (Emulates Turing’s enigma based on configurational states)

— [A concise history of computation](https://www.google.com/url?q=https://pron.github.io/computation-logic-algebra&sa=D&ust=1583943622552000)  (An extremely well-written account of computational logic history; includes Aristotle to Leibniz to McCarthy)

— [Great works in programming languages](https://www.google.com/url?q=https://www.cis.upenn.edu/~bcpierce/courses/670Fall04/GreatWorksInPL.shtml&sa=D&ust=1583943622552000)

— [Complexity Explorer](https://www.google.com/url?q=https://www.complexityexplorer.org/courses&sa=D&ust=1583943622553000)  (Best MOOCS on web on complexity science, AI, ML, computation)

— [Paul Graham Essays](https://www.google.com/url?q=http://www.paulgraham.com/articles.html&sa=D&ust=1583943622554000)  (People swear by it, so there must be something)

— [Akiyoshi Kitaoka’s Illusions](https://www.google.com/url?q=http://www.ritsumei.ac.jp/~akitaoka/index-e.html&sa=D&ust=1583943622554000)  (One of the foremost psychologist working on infotemporal cortex)

— [Joshua Horowitz](https://www.google.com/url?q=http://joshuahhh.com/&sa=D&ust=1583943622555000)  (Superb Resources on Mathematics and Graphics)

# YOUTUBE: Computation, Design, & Math

None of the videos mentioned here require any background. All of them take you very deep in terms of their contemplation

Videos

— [Bret Victor "Inventing on Principle"](https://www.google.com/url?q=https://youtu.be/PUv66718DII&sa=D&ust=1583943622557000)  (If Doug Engelbart and Alan Kay were the greats/visionaries of computing from last generation, We have Bret now)

— [The mother of All demos](https://www.google.com/url?q=https://youtu.be/yJDv-zdhzMY&sa=D&ust=1583943622558000)  (The evergreen and the legendary demo by Doug Engelbart and Team that revolutionized computing as we know it today)

— [Why are flowers beautiful?](https://www.google.com/url?q=https://youtu.be/gT7DFCF1Fn8&sa=D&ust=1583943622559000)  (David Deutsch on Objectivity of aesthetics over the prevalent notion of subjective notion)

—  [The Science Delusion](https://www.google.com/url?q=https://youtu.be/JKHUaNAxsTg&sa=D&ust=1583943622559000)  (The flaws of science as a religion)

— [Compositional Game Theory](https://www.google.com/url?q=https://www.youtube.com/playlist?list%3DPLRy_Pn1LtSpcq1AFVzZO6-l4Gt43QlYwR&sa=D&ust=1583943622560000)  (A superb intro to compositionality)

— [Russell Ackoff From Mechanistic Thinking to Systemic Thinking](https://www.google.com/url?q=https://youtu.be/yGN5DBpW93g&sa=D&ust=1583943622561000)  (Based on Chaos Theory)

— [Series on chaos with great production value](https://www.google.com/url?q=https://youtu.be/c0gDLEHbYCk&sa=D&ust=1583943622562000)  (Based on complexity and Chaos Theory)

— [Visual group theory based on the book by Needham](https://www.google.com/url?q=https://www.youtube.com/playlist?list%3DPLwV-9DG53NDxU337smpTwm6sef4x-SCLv&sa=D&ust=1583943622562000)

— [Topology and Geometry](https://www.google.com/url?q=https://www.youtube.com/playlist?list%3DPLTBqohhFNBE_09L0i-lf3fYXF5woAbrzJ&sa=D&ust=1583943622563000) (Best course on Topology & Geometry, Can approach Directly, Serious but extremely fun and intuitive!)

— [CRDT Primer - Defanging Order Theory](https://www.google.com/url?q=https://youtu.be/OOlnp2bZVRs&sa=D&ust=1583943622564000)  (Advantages of thinking in terms of order theory)

—  [Euclid’s Voyage into Chaos](https://www.google.com/url?q=https://www.youtube.com/watch?v%3Dr1Moj-Mew-k&sa=D&ust=1583943622565000)  (Based on complexity and chaos)

—  [Escher and Me with Roger Penrose](https://www.google.com/url?q=https://youtu.be/f7kW8xd8p4s&sa=D&ust=1583943622566000)  (The famous penrose and escher collaboration)

— [Programming since Egyptians](https://www.google.com/url?q=https://www.youtube.com/playlist?list%3DPLHxtyCq_WDLV5N5zUCBCDC2WqF1VBDGg1&sa=D&ust=1583943622566000)  (Neat stuff by Alexander Stephanov, The guy known for the “Elements of Programming”)

Interviews (OLDIES BUT GOLDIES)

— [Is emergence fundamental by Stu Kauffman](https://www.google.com/url?q=https://youtu.be/GVL2Y5z2jLU&sa=D&ust=1583943622567000)  (When stu speaks you listen)

— [A conversation with Bertrand Russell](https://www.google.com/url?q=https://www.youtube.com/watch?v%3Dfb3k6tB-Or8&sa=D&ust=1583943622568000)

— [Best lecture on Marcus Aurelius's by Prof Michael Surgue](https://youtu.be/5897dMWJiSM)

— [Face-to-Face with Carl Gustav Jung](https://www.google.com/url?q=https://youtu.be/2AMu-G51yTY&sa=D&ust=1583943622569000)

— [Aldous Huxley interviewed by Mike Wallace](https://www.google.com/url?q=https://youtu.be/alasBxZsb40&sa=D&ust=1583943622570000)

—  [A Documentary on Orwell](https://www.google.com/url?q=https://youtu.be/s6txpumkY5I&sa=D&ust=1583943622570000)

Random Videos

— [Joe Rogan Show with Naval Ravikant](https://www.google.com/url?q=https://youtu.be/3qHkcs3kG44&sa=D&ust=1583943622571000)

— [Joe Rogan Show with Eric Weinstein](https://www.google.com/url?q=https://youtu.be/X9JLij1obHY&sa=D&ust=1583943622572000)

# ONLINE INTERACTIVE BOOKS

Some of the resources below are extraordinary specifically the nature of code, complex analysis, crafting interpreters, category theory, sicp, euclid, and Interactive linear algebra both versions

— [Interactive Linear Algebra Hypertext book](https://www.google.com/url?q=http://textbooks.math.gatech.edu/ila&sa=D&ust=1583943622577000)  – Joseph Rabinoff

— [Euclid's Element digitized](https://www.google.com/url?q=http://c82.net/euclid&sa=D&ust=1583943622578000)  – Nicholas Rogeux

— [Interactive, Second to none reference on Complex Analysis](https://www.google.com/url?q=http://complex-analysis.com&sa=D&ust=1583943622579000)  – Juan Campuzano

— [Interactive Graph Theory](https://www.google.com/url?q=http://d3gt.com/index.html&sa=D&ust=1583943622580000)  – Avinash Pandey

— [Nature of Code: Neat online resource on graphics](https://www.google.com/url?q=http://natureofcode.com/book&sa=D&ust=1583943622581000)  – Daniel Shiffman

— [Interactive Linear Algebra](https://www.google.com/url?q=http://immersivemath.com/ila/index.html&sa=D&ust=1583943622582000)  – Jacob Strom

— [Interactive Music Theory](https://www.google.com/url?q=http://lightnote.co&sa=D&ust=1583943622582000)

— [Fractals and Complexity Science](https://www.google.com/url?q=http://wahl.org/fe/HTML_version/link/contents.htm&sa=D&ust=1583943622583000)  – Bernt Rainer Wahl

— [Learn about viruses in an interactive way](https://www.google.com/url?q=http://viruspatterns.com&sa=D&ust=1583943622584000)  — Hamish Todd

— [All-in-all explorable Interactive Resource](https://www.google.com/url?q=https://explorabl.es/&sa=D&ust=1583943622585000) — Nickey Case

— [Mathematics for Physics](https://www.google.com/url?q=http://mathphysicsbook.com&sa=D&ust=1583943622585000)

— [A nice online interactive site on waveforms](https://www.google.com/url?q=http://pudding.cool/2018/02/waveforms&sa=D&ust=1583943622586000)  – Josh Commeau

— [Modern OpenGL tutorial](https://www.google.com/url?q=http://learnopengl.com&sa=D&ust=1583943622587000)  – Joey De Vries

— [Free and opensource textbooks](https://www.google.com/url?q=http://danaernst.com/resources/free-and-open-source-textbooks&sa=D&ust=1583943622588000)  – Dana Ernst

— [Practical Typography: Form and Content](https://www.google.com/url?q=http://practicaltypography.com&sa=D&ust=1583943622589000)  – Butterick

— [Statistics and Probability](https://www.google.com/url?q=http://statlect.com&sa=D&ust=1583943622590000)  — Marco Taboga

— SICP - Structure and Interpretation of Computer Programs  – Gerald Jay Sussman

— [Crafting Interpreters](https://www.google.com/url?q=https://www.craftinginterpreters.com/&sa=D&ust=1583943622608000)  – Bob Nystrom

— [Dark Science](https://www.google.com/url?q=http://dresdencodak.com/2010/06/03/dark-science-01/&sa=D&ust=1583943622609000)  by Dresden Codak

— [Category Theory for Programmers](https://www.google.com/url?q=https://bartoszmilewski.com/2014/10/28/category-theory-for-programmers-the-preface/&sa=D&ust=1583943622609000)  by Bartosz Milewski

# INTERESTING GITHUB REPOSITORIES

— [Magic Book](https://www.google.com/url?q=https://github.com/magicbookproject/magicbook&sa=D&ust=1583943622610000)  – An open source self-publishing tool for publishing your book online

— [Google Docs Publisher](https://www.google.com/url?q=https://github.com/augnustin/google-docs-publisher&sa=D&ust=1583943622611000)  – The thing that I am using right now to share the doc as a website with you

— [Proselint](https://www.google.com/url?q=https://github.com/amperser/proselint&sa=D&ust=1583943622612000)  – An open source linter for correcting your prose (if and when you start blogging or writing)

— [Effects-Bibliography](https://www.google.com/url?q=https://github.com/yallop/effects-bibliography&sa=D&ust=1583943622612000)  – A nicely curated collection of all the theory of computational models

— [Digital Video Introduction](https://www.google.com/url?q=https://github.com/leandromoreira/digital_video_introduction&sa=D&ust=1583943622613000) – A neat introduction to digital video technology and compression in an accessible way with images and gifs

— [Advanced Math for Engineers](https://www.google.com/url?q=https://github.com/nicoguaro/AdvancedMath&sa=D&ust=1583943622614000)  – A so so ipynb for advanced math.

— [WebGL and GLSL](https://www.google.com/url?q=https://github.com/mattdesl/workshop-webgl-glsl&sa=D&ust=1583943622615000)  – Intro to WebGL using canvas and threejs

— [Napkin](https://www.google.com/url?q=https://github.com/vEnhance/napkin&sa=D&ust=1583943622615000)  – An advanced math book for high-schoolers ([Book can be found here](https://www.google.com/url?q=https://venhance.github.io/napkin/Napkin.pdf&sa=D&ust=1583943622616000). An excellent resource to learn higher mathematics)

— [Awesome DIY Software](https://www.google.com/url?q=https://github.com/cweagans/awesome-diy-software&sa=D&ust=1583943622616000) – Much like “[Projects from Scratch](https://www.google.com/url?q=https://github.com/AlgoryL/Projects-from-Scratch&sa=D&ust=1583943622617000)”, “[Project Based Learning](https://www.google.com/url?q=https://github.com/tuvtran/project-based-learning&sa=D&ust=1583943622617000)”, “[Build your Own X](https://www.google.com/url?q=https://github.com/danistefanovic/build-your-own-x&sa=D&ust=1583943622618000)”

— [Computer Science Resources](https://www.google.com/url?q=https://github.com/the-akira/Computer-Science-Resources&sa=D&ust=1583943622619000) – A neat collection of computer science resources

— [Neo: A Language agnostic scripting engine](https://www.google.com/url?q=https://github.com/i42output/neos&sa=D&ust=1583943622619000) – A scripting language that cross-compiles from all the major languages

— [Awesome Job Board](https://www.google.com/url?q=https://github.com/tramcar/awesome-job-boards&sa=D&ust=1583943622620000)  – A decent collection of jobs related to all compsci fields

— [Awesome FFmpeg](https://www.google.com/url?q=https://github.com/transitive-bullshit/awesome-ffmpeg&sa=D&ust=1583943622621000)  – May not be useful to you, but extremely good collection of all ffmpeg materials if you ever wanted to get into graphics programming openGL and create videos

— [Vim Galore: All things Vim](https://www.google.com/url?q=https://github.com/mhinz/vim-galore&sa=D&ust=1583943622622000)  – Nice, but only if you use Vim, which I do.

— [The practical linux hardening guide](https://www.google.com/url?q=https://github.com/trimstray/the-practical-linux-hardening-guide&sa=D&ust=1583943622623000)  – An all-in-all decent collection of all the commonly used linux tricks, resources etc

— [Self-teach Computer Science in 10 Years](https://www.google.com/url?q=https://github.com/ossu/computer-science&sa=D&ust=1583943622623000)  – The best resource if you want to teach yourself computer science ground up in a slow and elaborate manner. Very well done!

— [Papers we love](https://www.google.com/url?q=https://github.com/papers-we-love/papers-we-love&sa=D&ust=1583943622624000)  – This is what I was talking about the other day, They store the papers here.

— [Every Programmer should know](https://www.google.com/url?q=https://github.com/mtdvio/every-programmer-should-know&sa=D&ust=1583943622625000)  – I think I had already shared this with you while we were in HPE, but just in case

— [Hardware Effects](https://www.google.com/url?q=https://github.com/Kobzol/hardware-effects&sa=D&ust=1583943622626000)  – A nice repo demonstrating different vulnerabilites when you write bad code.

— [Visual Math Tools](https://www.google.com/url?q=https://github.com/prathyvsh/Visual-Math-Tools&sa=D&ust=1583943622626000)  – A nice repo containing visual math tools.

# PODCAST RECOMMENDATIONS

— [The Artificial Intelligence Podcast](https://www.google.com/url?q=https://open.spotify.com/show/2MAi0BvDc6GTFvKFPXnkCL&sa=D&ust=1583943622628000)  – Lex Fridman [[Youtube Video Playlist](https://www.google.com/url?q=https://www.youtube.com/playlist?list%3DPLrAXtmErZgOdP_8GztsuKi9nrraNbKKp4&sa=D&ust=1583943622628000)]

— [The Portal](https://www.google.com/url?q=https://open.spotify.com/show/3qv8BS1HzrgKpDnXSlYWWL&sa=D&ust=1583943622629000)  – Eric R. Weinstein [[Youtube Video Playlist](https://www.google.com/url?q=https://www.youtube.com/playlist?list%3DPLq9jO8fmlPee9ezOraOHAJ3g9Zh3V2F2G&sa=D&ust=1583943622630000)]

— [Making Sense](https://www.google.com/url?q=https://open.spotify.com/show/5rgumWEx4FsqIY8e1wJNAk&sa=D&ust=1583943622631000) – Sam Harris

— [The Drive](https://www.google.com/url?q=https://open.spotify.com/show/63AWQmsSnFNFHUqnRAOFtD&sa=D&ust=1583943622631000) – Peter Attia

— [The Knowledge Project](https://www.google.com/url?q=https://open.spotify.com/show/1VyK52NSZHaDKeMJzT4TSM&sa=D&ust=1583943622632000)  – Shane Parris (Farnam Street Fame)

— [Conversations with Tyler](https://www.google.com/url?q=https://open.spotify.com/show/0Z1234tGXD2hVhjFrrhJ7g&sa=D&ust=1583943622633000)  – Tyler Cowen

— [The Tim Ferriss Show](https://www.google.com/url?q=https://open.spotify.com/show/5qSUyCrk9KR69lEiXbjwXM&sa=D&ust=1583943622634000) – Tim Ferriss

— [Dan Carlin’s Hardcore History](https://www.google.com/url?q=https://open.spotify.com/show/72qiPaoDRf8HkGKEChvG5q&sa=D&ust=1583943622635000) – Dan Carlin

— [Daniel & Jorge Explain the Universe](https://www.google.com/url?q=https://open.spotify.com/show/5t4keSVD2zRQuUEtwuoq4V&sa=D&ust=1583943622636000) – Daniel and Jorge

— [History of Philosophy without Any Gaps](https://www.google.com/url?q=https://open.spotify.com/show/5NkIduNOSgSELCYIa4RaNq&sa=D&ust=1583943622637000)  – Peter Adamson

— [Indie Hackers](https://www.google.com/url?q=https://open.spotify.com/show/4ex8hmrHCPvPohKJb3wsuC&sa=D&ust=1583943622637000) – Courtland Allen

— [Philosophize This](https://www.google.com/url?q=https://open.spotify.com/show/2Shpxw7dPoxRJCdfFXTWLE&sa=D&ust=1583943622638000) – Stephen West

— [Rationally Speaking with Julia Galef](https://www.google.com/url?q=https://open.spotify.com/artist/62tC2LQ4dKzNEIAUFFuHmo&sa=D&ust=1583943622639000) – Julia Galef

— [The universe speaks in numbers](https://www.google.com/url?q=https://open.spotify.com/artist/62tC2LQ4dKzNEIAUFFuHmo&sa=D&ust=1583943622640000)  – Graham Farmelo

— [Naval Podcast](https://www.google.com/url?q=https://open.spotify.com/show/7qZAVw03FuurfYnWIWwkHY&sa=D&ust=1583943622640000) – Naval Ravikant

— [Freakonomics Radio](https://www.google.com/url?q=https://open.spotify.com/show/6z4NLXyHPga1UmSJsPK7G1&sa=D&ust=1583943622641000) – Stephen Dubner

# BOOK RECOMMENDATIONS (Non-Fiction)

All the books are linked to their respective amazon links; But just to give a heads up, all of them can be downloaded for free and loaded in Kindle, PlayBooks, iBooks by preserving all the functionality like flipping etc through b-ok/libgen. Some of them may be in public domain, so try Internet Archive or Project Gutenberg

And I have made sure to include only books that I’ve read, reviewed, and liked. Got many of these recommendations from twitter, goodreads, and reddit.

— [Gödel, Escher, Bach: An Eternal Golden Braid](https://www.google.com/url?q=https://www.amazon.com/G%25C3%25B6del-Escher-Bach-Eternal-Golden/dp/0465026567&sa=D&ust=1583943622643000)  – Douglas Hofstader

— [Antifragile: Things that gain from Disorder](https://www.google.com/url?q=https://www.amazon.com/Antifragile-Things-That-Disorder-Incerto/dp/0812979680&sa=D&ust=1583943622644000)  –  Nassim Nicholas Taleb

— [The beginning of Infinity](https://www.google.com/url?q=https://www.amazon.com/Beginning-Infinity-Explanations-Transform-World/dp/0143121359&sa=D&ust=1583943622646000) – David Deutsch

— [The book of Why](https://www.google.com/url?q=https://www.amazon.com/Book-Why-Science-Cause-Effect/dp/046509760X&sa=D&ust=1583943622646000)  – Judea Pearl

— [On the origins of objects](https://www.google.com/url?q=https://www.amazon.co.uk/Origin-Objects-Bradford-Book/dp/0262692090&sa=D&ust=1583943622647000) -- Brian Smith

— [Unflattening](https://www.google.com/url?q=https://www.amazon.com/Unflattening-Nick-Sousanis/dp/0674744438&sa=D&ust=1583943622648000) – Nick Sousanis

— [CODE](https://www.google.com/url?q=https://www.amazon.com/Code-Language-Computer-Hardware-Software/dp/0735611319&sa=D&ust=1583943622649000) - Charles Petzold

— [The Elephant in the brain: Hidden Motives in Everyday lives](https://www.google.com/url?q=https://www.amazon.com/Elephant-Brain-Hidden-Motives-Everyday/dp/0190495995&sa=D&ust=1583943622650000) – Robin Hanson

— [Myths to live by](https://www.google.com/url?q=https://www.amazon.com/Myths-Live-Joseph-Campbell/dp/0140194614&sa=D&ust=1583943622650000) – Joseph Campbell

— [Equations from God: Pure Mathematics and Victorian Faith](https://www.google.com/url?q=https://www.amazon.com/Equations-God-Mathematics-Victorian-Hopkins/dp/0801885531&sa=D&ust=1583943622651000) - Daniel J Cohen

— [Talking Nets: An oral history of Neural Nets](https://www.google.com/url?q=https://www.amazon.com/Talking-Nets-History-Neural-Networks/dp/0262511118&sa=D&ust=1583943622652000) - Edward Rosenfeld and James Anderson

— [The Embodied Mind: Cognitive Science and Human Experience](https://www.google.com/url?q=https://www.amazon.com/Embodied-Mind-Cognitive-Science-Experience/dp/0262720213&sa=D&ust=1583943622653000) – Franciso Varela

— [Indiscrete Thoughts](https://www.google.com/url?q=https://www.amazon.com/Indiscrete-Thoughts-Modern-Birkh%25C3%25A4user-Classics/dp/0817647805&sa=D&ust=1583943622653000) - Gian-Carlo Rota

— [The Mind’s I](https://www.google.com/url?q=https://www.amazon.com/Minds-Fantasies-Reflections-Self-Soul/dp/0465030912&sa=D&ust=1583943622654000) – Douglas Hofstader, Daniel Dennett

— [Modern History of Computing](https://www.google.com/url?q=https://plato.stanford.edu/entries/computing-history/&sa=D&ust=1583943622655000) – Paul Ceruzzi

— [Mind in Society](https://www.google.com/url?q=https://www.amazon.com/Mind-Society-Development-Psychological-Processes/dp/0674576292&sa=D&ust=1583943622656000) – Ls Vygotsky

— [The conquest of happiness](https://www.google.com/url?q=https://www.amazon.com/Conquest-Happiness-Bertrand-Russell/dp/087140673X&sa=D&ust=1583943622656000) - Bertrand Russell

— [The art of memory](https://www.google.com/url?q=https://www.amazon.com/Art-Memory-Frances-Yates/dp/1847922929&sa=D&ust=1583943622657000) – Francis A. Yates

— [The Philosophy of Artificial Life](https://www.google.com/url?q=https://www.amazon.com/Philosophy-Artificial-Life-Oxford-Readings/dp/0198751559&sa=D&ust=1583943622658000) – Margaret A. Boden

— [The Singularity is near](https://www.google.com/url?q=https://www.amazon.com/Singularity-Near-Humans-Transcend-Biology/dp/0143037889&sa=D&ust=1583943622659000) – Ray Kurzweil

— [A new kind of science](https://www.google.com/url?q=https://www.amazon.com/New-Kind-Science-Stephen-Wolfram/dp/1579550088&sa=D&ust=1583943622660000) – Stephen Wolfram

— [Superintelligence](https://www.google.com/url?q=https://www.amazon.com/Superintelligence-Dangers-Strategies-Nick-Bostrom/dp/1501227742&sa=D&ust=1583943622660000) – Nick Bostrom

— [Philosophies of India](https://www.google.com/url?q=https://www.amazon.com/Philosophies-India-Heinrich-Robert-Zimmer/dp/0691017581&sa=D&ust=1583943622661000) – Heinrich Zimmer, Joseph Campbell

— [Range: How generalist Triumph in a Specialized World](https://www.google.com/url?q=https://www.amazon.com/Range-Generalists-Triumph-Specialized-World/dp/0735214484&sa=D&ust=1583943622662000) – David Epstein

— [Mythologies](https://www.google.com/url?q=https://www.amazon.com/Mythologies-Roland-Barthes/dp/0374521506&sa=D&ust=1583943622663000) – Roland Barthes

— [The design of everyday things](https://www.google.com/url?q=https://www.amazon.com/Design-Everyday-Things-Revised-Expanded/dp/0465050654&sa=D&ust=1583943622664000) – Don Norman

— [Turing's Cathedral](https://www.google.com/url?q=https://www.amazon.com/Turings-Cathedral-Origins-Digital-Universe/dp/1400075998&sa=D&ust=1583943622664000) – George Dyson

— [Engines of Logic](https://www.google.com/url?q=https://www.amazon.com/Engines-Logic-Mathematicians-Origin-Computer/dp/0393322297&sa=D&ust=1583943622665000)/Universal Computer – Martin D. Davis

— [Logicomix](https://www.google.com/url?q=https://www.amazon.com/Logicomix-search-truth-Apostolos-Doxiadis/dp/1596914521&sa=D&ust=1583943622666000)  – A. Doxiadis, C. Papadimitriou

— [Zen and the art of motorcycle maintenance](https://www.google.com/url?q=https://www.amazon.com/Zen-Art-Motorcycle-Maintenance-Inquiry/dp/0060589469&sa=D&ust=1583943622667000) – Robert Pirsig

— [The problem of philosophy](https://www.google.com/url?q=https://www.gutenberg.org/files/5827/5827-h/5827-h.htm&sa=D&ust=1583943622668000) – Bertrand Russell

— [Problems of Knowledge and Freedom](https://www.google.com/url?q=https://www.amazon.com/Problems-Knowledge-Freedom-Russell-Lectures/dp/1565848098&sa=D&ust=1583943622669000) – Noam Chomsky

— [How the world works](https://www.google.com/url?q=https://www.amazon.co.uk/How-World-Works-Noam-Chomsky/dp/0241145384&sa=D&ust=1583943622670000) – Noam Chomsky

— [Manufacturing Consent](https://www.google.com/url?q=https://www.amazon.com/Manufacturing-Consent-Political-Economy-Media/dp/0375714499&sa=D&ust=1583943622670000) – Noam Chomsky

— [Surely You’re Joking Mr Feynman](https://www.google.com/url?q=https://www.amazon.com/Surely-Feynman-Adventures-Curious-Character/dp/0393316041&sa=D&ust=1583943622671000) – Richard Feynman

— [Six Easy Pieces](https://www.google.com/url?q=https://www.amazon.com/Six-Easy-Pieces-Essentials-Explained/dp/0465025277&sa=D&ust=1583943622672000) – Richard Feynman

— [Skin in the Game](https://www.google.com/url?q=https://www.amazon.com/Skin-Game-Hidden-Asymmetries-Daily/dp/042528462X&sa=D&ust=1583943622673000) – Nassim Nicholas Taleb

— [Zero To One](https://www.google.com/url?q=https://www.amazon.com/Zero-One-Notes-Startups-Future/dp/0804139296&sa=D&ust=1583943622673000) – Peter Thiel

— [The autobiography of Benjamin Franklin](https://www.google.com/url?q=https://www.amazon.com/Autobiography-Benjamin-Franklin/dp/1640320032&sa=D&ust=1583943622674000) – Benjamin Franklin

— [Infinite Powers](https://www.google.com/url?q=https://www.amazon.com/Infinite-Powers-Calculus-Reveals-Universe/dp/1328879984&sa=D&ust=1583943622675000) – Steven Strogatz

— [Letters from a Stoic](https://www.google.com/url?q=https://www.amazon.com/Letters-Penguin-Classics-Lucius-Annaeus/dp/0140442103&sa=D&ust=1583943622676000) – Seneca

— [Philosophical Investigations](https://www.google.com/url?q=https://www.amazon.com/Philosophical-Investigations-3rd-Ludwig-Wittgenstein/dp/0024288101&sa=D&ust=1583943622677000) – Ludwig Wittgenstein

— [Meditations](https://www.google.com/url?q=https://www.amazon.com/Meditations-Thrift-Editions-Marcus-Aurelius/dp/048629823X&sa=D&ust=1583943622677000) – Marcus Aurelius

— [Flow: The Psychology of Optimal Experience](https://www.google.com/url?q=https://www.amazon.com/Flow-Psychology-Experience-Perennial-Classics/dp/0061339202&sa=D&ust=1583943622678000) – Mihaly Csikszentmihalyi

— [A Mathematician’s Apology](https://www.google.com/url?q=https://www.amazon.com/Mathematicians-Apology-G-H-Hardy/dp/1466402695&sa=D&ust=1583943622679000) – G.H Hardy

— [How to create a human mind: The secret of human thought revealed](https://www.google.com/url?q=https://www.amazon.com/How-Create-Mind-Thought-Revealed-ebook/dp/B007V65UUG&sa=D&ust=1583943622680000) – Ray Kurzweil

— [The Guide for the Perplexed](https://www.google.com/url?q=https://www.amazon.com/Guide-Perplexed-Moses-Maimonides/dp/0486203514&sa=D&ust=1583943622681000) – Maimonides

— [What is Life? With Mind and Matter and Autobiographical Sketches](https://www.google.com/url?q=https://www.amazon.com/What-Life-Autobiographical-Sketches-Classics/dp/1107604664&sa=D&ust=1583943622681000) – Erwin Schrodinger

— [The Society of Mind](https://www.google.com/url?q=https://www.amazon.com/Society-Mind-Marvin-Minsky/dp/0671657135&sa=D&ust=1583943622682000) – Marwin Minsky

— [The timeless way of Building](https://www.google.com/url?q=https://www.amazon.com/Timeless-Way-Building-Christopher-Alexander/dp/0195024028&sa=D&ust=1583943622683000) – Christopher Alexander

— [Systems Thinking and Chaos – Simple Scientific Analysis on How Chaos and Unpredictability Shape Our World](https://www.google.com/url?q=https://www.amazon.com/Systems-Thinking-Chaos-Scientific-Unpredictability-ebook/dp/B07XY8XCXG&sa=D&ust=1583943622684000)  – A Rutherford

— [Flatlands: A Romance of Many Dimensions](https://www.google.com/url?q=https://www.amazon.com/Flatland-Romance-Dimensions-Thrift-Editions/dp/048627263X&sa=D&ust=1583943622684000) – Edwin A. Abbott

— [The Science of Conjecture: Evidence and Probability before Pascal](https://www.google.com/url?q=https://www.amazon.com/Science-Conjecture-Evidence-Probability-before/dp/0801871093&sa=D&ust=1583943622685000) – James Franklin
