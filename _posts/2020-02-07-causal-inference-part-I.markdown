---
layout: post
title: Causal Inference — A Guide to Causality Part I
comments: true
permalink: /causal-inference-part-I/
categories: Statistics
---

Most of us are well aware of the phrase "correlation is not causation" but only few of us are capable of seeing past our biases. In order to spot the biases, the bias detection engine in our brains will have to be trained to spot them. We must develop the ability to discern the difference between causation and correlation. It is not easy, the dynamics of the system do not allow for such a provision without going through the hard route of training oneself to peruse every piece of information we receive. And luckily for us, mathematicians and statisticians have already created a framework to work with, the study of causality through analysis of information, also known as Causal Inference. This is my attempt at compiling a guide that you can use to understand the science of causality.

Causal Inference is a sub-field in Information Theory and Probability Theory. But one doesn't have to be a statistician or a mathematician to go about understanding the basics of causality theory. Anyone with an interest in knowing about causation is perfectly capable of learning it. Learning the applications of causality theory can be real helpful for all us to navigate our lives better and also help us make better decisions throughout our lives. Before we start, this essay is more like a transcription of an [online course](https://www.coursera.org/learn/crash-course-in-causality/home/welcome) hosted in coursera on causal inference by U Penn. It is based on my notes on the aforementioned course. So, please feel free to checkout the course to get a better understanding of the topic.

Okay, now that we are done with the introduction and housekeeping, let us dive right in.

### Confusion over causality

The lack of clarity in identifying what is causality is our first and major roadblock in overcoming the biases. We as human beings have a tendency to confuse unrelated variables to causality. And in some instances, we may remain unaware of the mistake until we are made aware of it later by some new research. It is also possible that we may remain unaware of it all our life, attributing our success/failure to some completely irrelevant variable. The confusion can arise due to many things -- spurious correlations, anecdotes, science reporting, reverse causality etc.

#### Spurious Correlation

_Causally **unrelated** variables might happen to be highly correlated with each other over some period of time._

**For eg**. You could have a graph showing relation between wheat production in Asia and Divorce rate in Europe from 2004 to 2009, but we can be certain to some extent in this case that no matter how much closely related these lines are, there is very little chance for wheat production to affect the divorce rate in Europe. Since there is always some remote chance for two completely unrelated quantities to be correlated, this might be confusing if we don't understand causality.

![](https://knowledgecontinuum.files.wordpress.com/2019/09/chartgo-1.png?w=600)

#### Anecdotes

_People have **beliefs** about causal effects in their own lives._

**For eg**. Suzuka Okinawa lived to be 115 years old. She said the secret to her longevity was eating one bitter-gourd a day. And because that is one visible difference that separates her from others who do not live that long, she believes that the bitter-gourd is the cause of her longevity. But we do not know if it is causal at all and we can't know for sure unless we can eliminate her other dietary details, lifestyle details, environmental details etc. Hypothetically speaking, it is also possible that if she had not eaten one bitter-gourd a day, she could have lived longer for all we know.

#### Science Reporting

_Headlines often do not use the forms of the word cause, but do get interpreted causally._

**For eg.** "Diet high in red meat linked to inflammatory bowel condition, study suggests"

News headlines tend to use loaded words like "linked" that can mean many things. In our case, it could be that red meats cause the condition, it could also mean that it might just deteriorates the condition, or for that matter it could be possible that helps to get better. And when conjoined with the word study suggests, this can lead to confusions. One would need the details of the research to know better.

If you know about causality and know about the topic in question, you might ignore it attributing to wrong correlation but if you are really excited about it, you might share it with others without realizing the effects of it on others. Hence it is really important to know how the experiment is designed, how the information was collated, and other details involved in the research before making an well-informed decision.

#### Reverse Causality

Even if there is a causal relationship, sometimes the direction can be unclear. And here what we are talking about is that the relationship between two variable could be bi-directional.

**For eg**. urban green space and exercise.

-   Are physically active people more likely to prioritize living near green space?
-   Or, Does green space in urban environment cause people to exercise more?

The causal arrow in this case could go in either directions. In such cases, we need temporal data to solve the chicken and egg problem.

### How to one clear up the confusion?

The field of causal inference or causal modeling attempts to do so by proposing:

-   Formal definition of causal effects
-   assumptions necessary to identify causal effects from data
-   rules about what variables need to be controlled for
-   sensitivity analyses to determine the impact of violations of assumptions on conclusion

We will discuss all of these in detail below but before go further, we will look at some basic definitions and notations to understand the concept of causal inference.

We will describe and define **_potential outcomes_,** and we will also define **_counterfactuals_**. These are probably the most important thing in the field, and we will keep coming back to them every now and then to extend our understanding to other topics in causal inference as they form the basis of many other topics.

### Treatment and Outcomes

When we think about causal effects, what we are really thinking about is what are the outcomes pertaining to a treatment. There are a lot of names for treatment. Sometimes treatments are also called exposure as in something that you are exposed to, which can affect the outcome.

Suppose we are interested in the causal effect of some treatment A on some outcome Y. **Most of the time** in our essay we will only be looking at binary treatment paths and binary outcome, but in reality it can be non-binary too.

Treatment examples:

-   A = 1 if receive influenza vaccine; A = 0 otherwise.
-   A = 1 if take statins; A = 0 otherwise.

Outcome examples:

-   Y = 1 if develop cardiovascular disease within 2 years;
-   Y = 0 otherwise Y = time until death [an example of non-binary outcome i.e., time]

The reason we assign numeric value is when we analyse the treatments or outcomes, we need something that is computable in nature. You could also think real drug vs placebo, exposed vs unexposed, etc. But the field is really more complicated, as in there could be levels of exposure or a patient could be on multiple medications etc.

### Potential Outcomes

In the above paragraph, what we saw in the form of Y is called as observed outcome but potential outcomes are something that we would see under each possible treatment.

**Notation:** Ya is the outcome that would be observed if treatment was set to A = a. Note the superscript, It is to denote potential outcomes. So, in the previous case, the potential case before observing could have been Y0 and Y1.

Example 1: Suppose treatment is influenza vaccine and the outcome is the time until the individual gets the flu.

Y1: time until the individual would get the flu if they received the flu vaccine

Y0: time until the individual would get the flu if they did not receive the flu vaccine

Example 2: Suppose the treatment is regional (A=1) versus general (A=0) anesthesia for hip fracture surgery. The outcome (Y) is major pulmonary complications.

Y1: equal to 1 if major pulmonary complications and equal to 0 otherwise, if given regional anesthesia

Y0: equal to 1 if major pulmonary complications and equal to 0 otherwise, if given general anesthesia

### Counterfactuals

Counterfactual outcomes are the outcomes that would have been observed, had the treatment been different.

Sometimes counterfactuals and potential outcomes could be used interchangeably but we will see that there are some differences. In this case, what we see is that the data has already been collected, outcome has already been observed, but we try to hypothesis what could have happened in some alternative scenario.

For example:

-   If my treatment was A=1, then my counterfactual outcome would be Y0
-   If my treatment was A=0, then my counterfactual outcome would be Y1

Let us understand this by using a real life example of influenza vaccine.

Did influenza vaccine prevent me from getting the flu?

Notice that in wording that we are already implying that the vaccine has already been given.

**What actually happened:**

-   You got the vaccine and did not get sick.
-   Your actual exposure was A=1 (meaning, you got the vaccine)
-   The observed outcome was Y= Y1

**What would have happened(contrary to fact):**

-   Had you not gotten the vaccine, would you have gotten sick?
-   Your counterfactual exposure is A=0
-   The counterfactual outcome is Y0

### To be continued...

- 📝 Link between Counterfactuals and Potential Outcomes
- 📝 Confounding and Directed Acyclic Graphs in Causality
- 📝 ATE, Matching, and Propensity Scores   
- 📝 IPTW - Inverse Probability of Treatment Weightage, Estimators
- 📝 Instrumental Variable Methods