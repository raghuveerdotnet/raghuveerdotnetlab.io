---
layout: post
title: How to improve your attention span and its benefits on mental well being?
comments: true
permalink: /how-to-improve-your-attention-span-and-its-benefit-on-mental-well-being/
categories: Practical Philosophy Meditation
---

William James, The father of American psychology in his groundbreaking book from 1890 “The principles of psychology” wrote

> “Everyone knows what attention is. Attention is the taking possession of the mind, in clear and vivid form, of one out of what seem several simultaneously possible objects or trains of thought. Focalisations, concentration of consciousness are of its essence. It implies withdrawal from some things in order to deal effectively with others”
> 
> <cite>---William James, Father of Modern Psychology</cite>

And since William James’ time, there have been numerous researches conducted on this very topic and we have a lot more understanding now than ever in the history of mankind about what attention is. And still we complain about how distracted we are, and not to mention its indirect consequences like an unending list of mental health issues ranging from depression, anxiety, and stress to an altogether separate list of innumerable tongue-twisting phobias. In fact, one of the major reasons for writing this essay was to speak about the indirect benefits of having an improved attention span on one’s psychological well-being. Having been a victim of several of those issues with varying severity at multiple points in my life, I thought if my experience could help at least a fraction of the suffering population, it would be one of the greatest accomplishments of my life. Also, you don’t have to take my words on faith, if you are skeptical, look at the various researches(_below and elsewhere_) on this and decide for yourself but until you do that here is my take.

Attention is, in fact, a very important concept in the field of psychology. And it only gets deeper and more difficult as you keep digging in. In fact, psychology categorizes attention into different types and if you keep going, you will find all sorts of theories weaved around those types but not much of that is useful for us today, what we want is the crux of what attention is, what we can do to improve it and on a more relatable level how it can help us in alleviating at least some of our miseries . So let us just jump straight into it.

To quote William James further, he says

> “The faculty of voluntarily bringing back a wandering attention, over and over again, is the very root of judgment, character, and will. No one is compos sui [master of himself] if he have it not. An education which should improve this faculty would be the education par excellence. But it is easier to define this ideal than to give practical directions for bringing it about.”
> 
> <cite>---William James, Psychology: Briefer Course</cite>

So simple and easy, yet so complex and difficult isn’t it. The key phrase to note here is the very first line of the quote – _“bringing back a wandering attention, over and over again”_ – this only goes on to prove that attention is not a passive state of not getting distracted but a very active and effortful act of coming back from the distraction and beginning over and over again.

At a more deeper level, attention can also be thought of as a skill that once acquired can weed out the abstractions from life. For example, most of us who have gone through at least some sort of difficult phase in our lives know that it is outrageously difficult to define the cause behind the emotion. One is sad but does not necessarily know why, One feels awkward but cannot see past the awkwardness and One is angry only to realize hours or days later that it was useless. It is with most of us that we tend to ruminate on the adjectivized thought for days together only to make irrational decisions later based on those same emotions and then regret it.

Imagine, if you could direct your attention towards these emotions when they arise and be able to deconstruct them and see them in their most naked form. It helps you reason the abstraction and gives you the ability to see what sadness is – a useless layer at the top covering other reasonable layers beneath it and so is every other negative emotion.

Being attentive does not only help with managing the emotional abstractions but also it helps with the physiological abstractions. Let us say if someone is suffering from immense pain and also, is constantly worried about the possible causes of the pain, wouldn’t it be nice to attend to the fact that suffering is only being extended from the physiological realm to the psychological. It only raises a question of what makes it so difficult for the wandering mind to not see that the pain has already manifested and rumination is just an unattended guest.

To be honest, these issues are too multivariate for anyone to address them entirely based on inductive reasoning but one conclusion that can certainly be inferred through inductive reasoning is lack of attention is definitely one of the cause of unnecessary suffering if not the only one. This reminds of a quote by Sam Harris on suffering which goes something like:

> “If you are suffering, you are not paying attention to your thoughts”
> 
> <cite>---Sam Harris, Neuroscientist and Philosopher</cite>

and I think it captures very precisely the essence of the article here. So, the next time you catch yourself pondering over unnecessary thought, instead of letting it run wild and getting completely consumed by it, try to pay attention to the thought. And just do it next to next time and next to next to next time and so on until your tiny little gray mass makes it a routine to bring the wandering mind back over, and over again without the need for your conscious effort.

See ya until next time and yeah, keep paying attention 🙂

----------

## References

James, William. (1890). *The Principles of Psychology*

Prof. Davidson, Richard . (Talks & Papers). *(William James and Vilas Professor of Psychology and Psychiatry at the University of Wisconsin–Madison and the Founder and Director of the [Center for Healthy Minds](https://www.google.com/url?q=https%3A%2F%2Fcenterhealthyminds.org%2F&sa=D&sntz=1&usg=AFQjCNFddIFtDdfNYVI2B2g95iR8mxi19Q))*
> - [Value of Attention in Building Resilience](https://youtu.be/HeBpsiFQiTI) *[Talk]*
> - [Brief Trainings to Buffer Against Acute Stress Effects](https://www.google.com/url?q=https%3A%2F%2Fcenterhealthyminds.org%2Fscience%2Fstudies%2Fbrief-trainings-to-buffer-against-acute-stress-effects&sa=D&sntz=1&usg=AFQjCNG0w1A0QsFodzlm4_L7l5xioniCMw) *[Paper]*
> - [Cognitive Control and the Regulation of Emotion, Attention and Pain](https://www.google.com/url?q=https%3A%2F%2Fcenterhealthyminds.org%2Fscience%2Fstudies%2Fcognitive-control-and-the-regulation-of-emotion-attention-and-pain&sa=D&sntz=1&usg=AFQjCNGVo6FLKEL6-trL40AO5BNZdaRKHQ) *[Paper]*
> - [Healthy Minds](https://www.google.com/url?q=https%3A%2F%2Fcenterhealthyminds.org%2Fscience%2Fstudies%2Fhealthy-minds&sa=D&sntz=1&usg=AFQjCNG3z4ivkmXbVkDyt4HzlQunsTZgpQ) *[Paper]*
> - [Conscious and Unconscious Emotional Processing](https://www.google.com/url?q=https%3A%2F%2Fcenterhealthyminds.org%2Fscience%2Fstudies%2Fconscious-and-unconscious-emotional-processing&sa=D&sntz=1&usg=AFQjCNGJesnq45j_ji6s2cGAd0hmL9N2oQ) *[Paper]*
> - [Large-Scale Analyses of the Effectiveness of Mindfulness-Based Interventions](https://www.google.com/url?q=https%3A%2F%2Fcenterhealthyminds.org%2Fscience%2Fstudies%2Flarge-scale-analyses-of-the-effectiveness-of-mindfulness-based-interventions&sa=D&sntz=1&usg=AFQjCNG2nFfRYNo9Rk9uL6CBIXAMATJ27g) *[Paper]*