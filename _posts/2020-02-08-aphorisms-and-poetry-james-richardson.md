---
layout: post
title: Aphorisms and Poetry - "James Richardson"
comments: true
permalink: /aphorisms-and-poetry-james-richardson/ 
categories: Philosophy
---

Has there ever been a form of communication so effective as poetry, in that, one could convey the feelings without the need for explaining it? I mean when you think of it, things like music and painting do come to mind, but let me be more specific here. Has there ever been a form of **_written_** communication so succinct and so full of pithiness that it evokes emotions so powerful that the quest to understand them ends the moment you begin to think of understanding them? If I were to wager a guess here, I think the only form that comes close to poetry is probably epigrams or aphorisms; I mean call it whatever you want to, but I believe even those are just poetry in disguise. So what is it that makes them so powerful? so potent? --- is it their melancholic nature? or is it the profoundness that they introduce unto you? or maybe it is the unintelligible yet beautiful depiction of the life that they portray. What is it? You can't tell, can you? So couldn't I when I first read **Vectors: Aphorisms and 10 Second Essays** by James Richardson.

I mean I could be exaggerating here when I call this a very profound book, for I have not read very many poetries in my life, but what I definitely have done is I have embarked on a quest to understanding the meaning of life, and I have done it multiple times only to fail miserably every time. So this should give you some idea about my interest in the pursuit of meaningful yet meaningless voyages. And I can, without doubt, tell you that this book by James was one such for him and will be for you too. 

This is a book of aphorisms that is written by a poet known for his melancholic insights---a combination made in heaven---so for what it matters, I think it is my duty to warn you of the ride you are going to take into the bottomless pit of the emotions so evocative that you will want to sit and just enjoy them. And now that I have warned you, let me give you a glimpse of some of my favorite aphorisms from this wonderful book.

I liked some of them more than the others, maybe because I could relate more to them, but I promise the book in total won't disappoint you. And more importantly, do not try to read this post in one sitting as you would with other more prosaic pos:w
ts of mine, for this isn't even my post, it is a compilation of my favorite aphorisms by James Richardson.

### The highlighted ones i.e, the more liked ones

> "I say nothing works anymore, but I get up and it's tomorrow."

> "We search for things we cannot resist as if desperate to prove we still have passions. Addiction is our metaphor. I give it up."

> "Beware of knowing your virtues; you may lose them. Beware of knowing your vices; you may forgive them."

> "Ah, what can fill the heart? But then, what can't?"

> "Who would be slave to his passions if they did not also feel like freedom?"

> "When your eyes widen in the dark, what is trying to get out?"

> If you change your mind, you are free. Or you were.

> "I never go to the mirror unless there’s something I am hoping not to see."

> "By looking for the origins of things we deceive ourselves about their inevitability. Things that did not happen also have origins."

> "The ambitious: those who have to work longer to find a place where it seems safe to stop."

> "He always wants to know what the best is so he can be superior to everything at once by seeing that the best isn’t perfect."

> "Minds go from intuition to articulation to self- defense, which is what they die of."

> "What is most remarkable about dreams is not their bizarreness but how rarely we experience them as bizarre. They must contribute silently to our faith that we could live through anything"

> "While I stand on line, my face goes slack as I daydream. When it’s my turn, the teller looks at me, frightened. What has he seen there?"

> "Who breaks the thread, the one who pulls, the one who holds on?"

> "Writing is like washing windows in the sun. With every attempt to perfect clarity you make a new smear."

> "The best way to get people to do what you want is not to be too particular about what you want them to do."

> "When you laughed at me, I could have been free, but instead I clung to my imprisonment."

> "The mind notices it exists when it gets in its own way, as two strands
have to get in each others’ way to make a knot. What's important in I
think, therefore I am is less the statement than the stumble."

> "If I belch at the table I am embarrassed. But not so much if no one
hears. Or if you pretend not to have heard. Or! pretend I do not know
you heard. Or you pretend you do not know I know."

> "Competition and sympathy are joined at the root, as may be seen in the game My grief is greater than yours, which no one can keep himself from playing."

> "Fewer regret wasting a year than never having wasted one."

> "A secret is the illusion that confessing it would make a difference."

> "Red photons have the lowest energy, blue the highest. Which color then for blood, water? Body, sky? Passion, distance?"

> "Even your great deeds are not all yours. There are days you are someone who couldn't have done them, or someone who wishes you hadn't."

> "That gentle, harmless drug that would make me permanently
happier? I would refuse it.After all, I can’t tell myself from my limits. It would be like dying for a
great cause: nothing of me would be left to know what I’d done. And I am no hero."

> "Each of the tastes, each of the colors is a marvel. All of them together are nothing."

> "A screwdriver is for screws. When you pry open a paint can with it, you have committed metaphor, which is the second use of things, their will gone. As for us, since we don’t know what our purpose is, all we do is metaphorical."

> "I could explain, but then you would understand my explanation, not what I said."

> "The best disguise is the one everyone else is wearing."

> "Big decisions threaten to narrow us, as if we would no longer be,
after choosing, the person who could have chosen otherwise.
Looking into the future, we see ourselves completely defined by one
choice—to be someone’s lover, to take such and such a job. We
become flat characters in a novel, and die a little."

> "Nothing so hampers creativity as having all the right tools"

> "The best time is stolen time."

> "I lied. And my embarrassment was so great that I changed everything else to make the lie true."

> "Iam hugely overpaid. Except compared to the people I work with."

> "What's thinking? You live in a grandly appointed house, but spend all
your time rummaging in the attic for any little trinket you hadn’t
known was there."

> "When I am trying to write I turn on music so I can hear what is
keeping me from hearing."

> "Writing a book is like doing a huge jigsaw puzzle, unendurably slow
at first, almost self- propelled at the end. Actually, it’s more like doing
a puzzle from a box in which several puzzles have been mixed.
Starting out, you can’t tell whether a piece belongs to the puzzle at
hand, or one you've already done, or will do in ten years, or will
never do."

> "All work is the avoidance of harder work."

> "How we struggle to articulate all we don’t know, how we fear coming to the end of it."

> "There, all along, was what you wanted to say. But this is not what
you wanted, is it, to have said it?"


The others that I liked:

`Note`: This is not all of them, do read the book to find what I may have missed. 😇

## On Happiness and Melancholy

> "Ah, what can fill the heart? But then, what can't?"

> "Despair says I cannot lift that weight. Happiness says, I do not have to."

> "What clings to good moments, or labors to repeat them? Not happiness, which is what lets you let them go."

> "Despair says It’s all the same. Happiness can distinguish a thousand Despairs."

> "If you say All is well, I believe you. If I say All is well, I'm abbreviating."

> "Absence makes the heart grow fonder: then it is only distance that separates us."

> "Who gives his heart away too easily must have a heart under his heart."

> "At first life is too slow, so we take to books. In the novel children
grow up in a sentence and a young man wakes up gray and over.
Also in life. And then we open books to slow down what we missed."

> "Happiness is a sad study."

## On Life, Hope, Reason, and Uncertainty

> "The will is weak. Good thing, or we'd succeed in governing our lives with our stupid ideas!"

> "Of all the ways to avoid living, perfect discipline is the most admired."

> "The worst helplessness is forgetting there is help."

> "We wouldn't take so many chances if we really believed in chance."

> "Selflove, strange name. Since it feels neither like loving someone, nor like being loved."

> "A belief is a question we have put aside so we can get on with what we believe we have to do."

> "If you reason far enough you will come to unreasonable conclusions." 

> "Music is the highest art, no question. But literature is a friendlier
one. It depends on us more, bores us more quickly, can’t go on if we
don't, can’t stop saying what it means, can’t stop giving us
something to forgive."

> "Language cannot be exact enough to prove anything absolutely or
rule anything out. Past a certain point, more precision in argument
becomes less, not more, scientific, like measuring the diameter of a
proton to six significant figures with a yardstick. Thinkers, whatever
they pretend, count on our mercy. Perfect rigor proves nothing but
distrust."

> "The first abuse of power is not realizing that you have it."

> "There are crimes I don’t commit mainly because I don’t want to find out I could."

> "The head learns quickly and forgets. The heart learns slowly and, since it cannot forget, must betray itself to move on."

> "The passions, it has been said, are the only orators that convince,
meaning they overcome reason. No, the passions are what
reasoning was invented for. The drives were nature's first provision:
thinking was added later, to get us around the world’s obstacles to
them. When we find ourselves arguing internally, the obstacle is not
reason but some other passion."

> "I know which disasters could be mine by how dangerous pity is."

> "The procrastinator dreads beginning, the workaholic, ending. Each
inhabits a parenthesis fending off Time. Every lit cigarette is a
bridge over Time’s emptiness, each quest or project or hope. Even
dread of death is just an- other way to make Time a story we can
bear to listen to."

> "Tragic hero, madman, addict, fatal lover. We exalt those who cannot escape their fates because we cannot stay inside our own."

> "To know, you just have to know. To believe, you have to make others believe."

> "I believe in nothing. But I’d prefer that you did not agree with me."

> "I don’t know what’s meant by know thyself, which seems to ask a
window to look at a window. I aspire to know when best to walk or
eat, which music I need, and how to keep myself sitting as I am now,
stubbornly enraptured with doing practically nothing. These are like
the things people learn who have to start persnickety cars in the
cold, or get the most out of exhausted fields."

> "Every life is allocated one hundred seconds of true genius. They might be enough. If we could just be sure which ones they were."

> "Every December the real cold may never come. Every March it may never go away."

> "Later you will not want it. Does that mean you should take it now or let it go?"

> "When you walk from a dark house into noon, you know it is brighter,
but you do not feel it as a hundred times brighter, which it may well
be, or a thousand times louder. The death of a thousand is not a
thousand times more painful than the death of one. The mind is
buffered, lest the ordinary minutes tear us apart, and perhaps in this
is the root of all generaliza- tion and abstraction. The extremes of joy
and sorrow are easily reached, even in the same hour. What we call
“perfect happiness” and “perfect misery” have less to do with
intensity than with duration and reliability. They are matters of hope."

> "Worry wishes life were over"

> "Surviving another argument we find that what kept us apart was fear of separation."

> "Back then I wanted to be right about my estimate of my abilities. Now I want to be wrong."

> "First he gathered what he needed. Then he needed to keep gathering what he used to need."

> "Why can we read character in a face? Partly because the child with
that face was treated as the character that seemed to belong to it,
and so acted it and grew into it. One comfort of middle age is seeing
around us the dozens who have finally grown beyond what their
faces and indeed their bodies said they were."

> "The lies I was ready to tell also count against me."

> "Of our first few years we remember nothing: experience only slowly
gives us the power to be formed by experience. If this were not true,
our characters would be completely determined by our infant hours
of confusion, pain and helplessness, and we would all be the same.
For her first six months my daughter cried continuously, who knows
why. Yet she is as happy and trusting and kind as if all that had never
happened. It never did."

> "As hard as other people are to talk to, I’m glad I don’t have to sit next to myself."

> "Embarrassment and guilt console us. They imply we have a purer
self that we have some- how betrayed. Regret, too, is a disguise of
hope, convincing us that things could have gone better, and
therefore that they may."

> "We have secrets from others. But our secrets have secrets from
us."

> "Some of my secrets everybody knows, but not that they are secrets."

> "What a relief when you have finally maneuvered yourself into a
position where anyone would agree the only thing left to do is tell the
truth."

## On Wisdom and Profundity

> "Pity the man who knows everything, for he has to fear surprise."

> "Greater than the temptations of beauty are those of method"

> "It’s easy to renounce the world till you see who picks up what you renounced."

> "Why would we write if we’d already heard what we wanted to hear?"

> "Looking back: youth, innocence, energy, desire. But none of it’s as amazing as all we were sure we had to do."

> "In clearing out files, ideas, hopes, throw away a little too much. Pruning only dead wood will not encourage growth."

> "I say I have no self-knowledge, but I know which things I will never tell you."

> "Even to say I believe nothing how much you have to believe."

> "The man who sticks to his plan will become what he used to want to be."

> "You can't pretend you're just watching the actors. Someone a little further away will see you acting the part of a watcher."

> "I walk up the drive for the morning paper and find myself musing, as if the news were fiction, Marvelous that they think of all this, so
deadpan strange. Nothing is so improbable as the truth. If the day’s
headlines hadn’t already happened, they would not happen."

> "The hard of hearing cannot tell their voices are loud."

> "If it can be used again, it is not wisdom but theory."

> "In the long run, the single sin is less of a problem than the good reasons for it."

> "To be sincere is one thing. To practice Sincerity is to burden everyone else with believing you."

> "The great consolation of righteousness is never having to worry whether youre a bore."

> "Time heals. By taking even more."

> "What we have is given again and again. It must be so, since not to have is to be continually denied."

> "All but the most durable books serve us simply by opening a window
on all we wanted to say and feel and think about. We may not even
notice that they have not said it themselves till we go back to them
years later and do not find what we loved in them. You cannot keep
the view by taking the window with you."

> "The mind that’s too sensitive feels mostly itself. A little hardness makes us softer for others."

> "Those who are too slow to be intelligent deserve our patience, those who are too quick, our pity."

> "Imagination is the only true waste: the rest is producing and
consuming. And much of what passes for imagination is only
obsession, which is just another kind of productivity, giving you what
you want again and again and again."

> "To think yourself incapable of crime is one failure of imagination. To think yourself capable of all crimes is another."

> "When it rains you discover which things you did not want out in the rain."

> "We are never so aware of how we look to others as when we have a great secret. Vanity always thinks it has a great secret."

> "The best way to know your faults is to notice which ones you accuse others of."

> "To condemn your sin in another is hypocrisy. Not to condemn is to reserve your right to sin."

> "Some things, like faith, cheer, courage, you can give when you do not have them."

> "What revolutions? We rush to mistake the multiplication of little
desires and little fulfillments for change because we fear nothing
ever changes."

> "No criticism so sharp as seeing they think you need to be flattered."

> "Envy, bitterness, disdain: failure to be alone."

> "The knife disappears with too much sharpening."

> "Turn on the light and you will see, but it will not be darkness that you see."

> "Do not wait for a clear desk or a windless day."

> "No garden without weeds? No weeds without gardens."

> "A truth is said over and over until it’s heard once, then unheard because it is too true."

> "Pleased is pleasing."

> "Believe everything a little. The credulous know things the skeptical do not."

> "No matter how much time I save, I have only now."

> "Embarrassment is the greatest teacher, but since its lessons are
exactly those we have tried hardest to conceal from ourselves, it
may teach us, also, to perfect our self-deception."

> "If you cannot heal your wound, praise its faithfulness."

> "Truth is like a virus. I fight it off, but it changes in other bodies and
returns in a form to which Iam not immune."

> "Some things should be fixed, others left broken."

> "You can't smell what the guests smell."

> "When you suspect me of a sin I did not commit, I am innocent of those I did."

> "Listen. But not too carefully, since what I’m saying is not exactly what I mean."

> "That the bookstores divide into romance and mystery suggests the two most powerful fantasies are someone to love and someone to blame."

> "Only half of writing is saying what you mean. The other half is preventing people from reading what they expected you to mean."


> "The great liars are credulous; they have convinced
themselves first of all."

> "Tell the truth lest it become too true."

> "I’m sitting here bored, trying to understand. No: trying to remember
that everything is a complete mystery."

> "Birds are amazing, newspapers, stoves, friends. All that happens is
amazing, if you think about it. All that doesn’t happen is even more
amaz- ing, because there’s so much more of it. Only habit keeps us
from seeing all this. Habit is really amazing."


## Optimism and Pessimism

> "No gift is ever exactly right for me. But why do I suppose the gift is for me, or my gratitude for the giver?"

> "Pessimists live in fear of their hope, optimists in fear of their fear."

> "It's not that reason kills faith; reason is the lesser faith that steers us when we have already lost a greater one."

> "The tyrant has first imagined he is a victim."

> "The worst part of fear is not knowing what to do. And often you only
have to ask What would I do if I were not afraid? to know what to do,
and do it, and not be afraid."

> "I scorn those who believe more, and envy them, pity those who believe less, and fear them."

> "Sometimes I hate beauty because I don’t have any choice about loving it. I must be wrong in this, but whether because I take freedom too seriously, or love, I cannot tell."

> "The shortest distance between two points is deciding they are the same."

> "The obvious was not necessarily obvious before you heard it."

> "Those who pride themselves on always telling the truth generally concentrate on truths more painful to others than to themselves."

> "When my friend does something stupid, he is just my friend doing something stupid. When I do something stupid, I have deeply betrayed myself."

> "There is no future till you underestimate the past."

> "If we remembered even a fraction of our million tiny plans, our whole lives would be regret at their failure."

> "That my youth was youthless once plagued me. By now it is an advantage: I waste no time on nostalgia."


## On Success, Failure, and Education

> "Success is whatever humiliation everyone has agreed to compete for."

> "Do we return again and again to our losses to get back what we had or lose what remains?"

> "On what is valuable thieves and the law agree."

> "Succeed and the world becomes just."

> "Education is so slow, disorganized, accidental that sometimes it
seems I could have relaxed for forty years and then learned
everything | know in a few months of really efficient study. Then
again, I’m not sure what I know."

> "Of very few subjects can it be said that the order in which we would teach them is the order in which we really learned them. The beginning is what we first see when we get to the end and turn around."

> "Do not blame the fire for knowing one thing."

> "The mistakes I made from weakness embarass me far less than those I made insisting on my strength."

> "The silly, incompetent dead, so much they forgot to tell us about how
to live! All we can learn from their books is that others have found
their own way and we may yet."

> "Once bitten, twice shy, it is said. Yet we make the same mistakes
over and over. Nature knows it is less important to escape pain than
to avoid exceptionless rules.

> "For one who needs it, praise is pity."

---

## References

Richardson, James. (2001). *Vectors---Aphorisms and Ten-Second Essays*


<br/>



