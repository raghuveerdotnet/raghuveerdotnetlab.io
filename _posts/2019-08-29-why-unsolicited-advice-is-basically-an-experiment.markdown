---
layout: post
title: Why unsolicited advice is basically an experiment
comments: true
permalink: /why-unsolicited-advice-is-basically-an-experiment/
categories: Practical
---

I’ve come to realize that unsolicited advice is basically an experiment conducted on the _“advisee”_ by the _“adviser”._ It is possible that the _“adviser”_ isn’t even aware of this being an inherent motive but I think it is largely the effect of culture that has kept them from noticing it and when I say culture, I mean global. It is also possible that the external intent is good i.e., to help the _“advisee”_ overcome the issue they are facing but the internal intent usually is to see how the advice plays out so that it can be used elsewhere with greater amount of confidence. Basically, a rewarding transaction that also makes them immune to the undesirable effects of the advice. _According to me, there are only six kinds of people who give unsolicited advice_ .

> **first**: the individuals who themselves have been through the exact same or a similar situation and have been able to recover, so they preach the exact same;
> 
> **second**: the individuals who have been through the exact same or a similar situation and have been able to recover, so they extrapolate and give you what they think might work for you(**the unwitting experimentalists**);
> 
> **third**: the individuals who have been through the exact same or a similar situation but none of their efforts have worked for them, so they try to unwittingly experiment on you to see if they could somehow be helped by it(**the unwittingly evil experimentalists**);
> 
> **fourth**: the individuals who have never been through anything even remotely similar to your situation but they out of their acquired knowledge on the topic through witnessing someone else’s journey they know or by being a well informed person on that issue try to advise and see if it works for you(**the witting experimentalists**);
> 
> **fifth**: the individuals who have never been through anything even remotely similar to your situation and also they haven’t had any first-hand knowledge of it but still try to use their existing knowledge to create an experimental amalgam of advice to try out on you(**the wittingly evil experimentalists**);
> 
> lastly, these are the kind of individuals who knowingly try to benefit off of you by selling you advice that they themselves would never have followed even in the direst of situations (**the truly evil experimentalists**).

I believe that it is perfectly okay to follow the first, second, and the fourth kind; and even in some cases the third kind but the last two are extremely dangerous and can be very difficult to spot, for they can disguise themselves as one of the top four types and trick you into buying their advice. Also the inherent problem with such advice is, no matter which category the individual belongs to, they are completely out of the game once they start the experiment and if the experiment by any chance yields undesirable results, these individuals are also completely absolved of all blame, for they are just some innocent bystanders who tried to help you. So, I think it is usually a good guiding heuristic to talk to people who have been through such situations and have a good amount of knowledge on that topic and have come out of it by following their own advice(if you find them, do let me know. Caution: these are mythical creatures). Jokes aside, just avoid people who enjoy giving advice.

Ironically, this is also a form of advice that you never asked for but honestly this is something I have experienced first hand. At the end of the day, this is also an opinion piece, so I could be biased in my analysis due to the kind of experiences that I’ve had, so take it with a pinch of salt but do show caution when something comes your way despite you not seeking it.

As usual enlighten me with a counter viewpoint(politely) and until then be happy 🙂
