---
layout: post
title: About Me
comments: true
permalink: /aboutme/
---

<center><img src="/assets/img/profile.png" height="120px" width="120px"/></center>

👨🏼‍💻 I am a software engineer with expertise in System Programming, and interest in Computer Graphics and Compiler Design.

📖I like to read(voraciously), ✍🏽write, and 💻code. I write essays/articles on everything I like, especially philosophy, psychology, programming, and mathematics.

🧘I meditate. I am a practitioner of meditation technique known as insight/mindfulness meditation (Vipassanā).

💻 [This](https://github.com/raghuveerdotnet)(GitHub) is my coding scratchpad if you are interested.
    
Although I am married to computer science and it is still my first love, I also wish to explore and learn as many things as possible and as long as possible. Hence the name: Knowledge Continuum.

If you would like to know my thought process, I host my notes publicly here: [NOTES](https://notes.raghuveer.net) ([notes.raghuveer.net](https://notes.raghuveer.net))

---

## Reach out to me

🐦 [Twitter](https://twitter.com/raghuveerdotnet) – Shoot me a dm or just tweet @raghuveerdotnet. I will try to reply as soon as I can. Sadly, I am not available on any other social media sites. I am not really into professional social media circle but anyway here is my [linkedin](https://www.linkedin.com/in/sraghuveer/) just in case.
    
🚶🏽 Bangalore is a huge city, so I can’t promise but If you happen to be in Bangalore by chance and want to chat, drop an email([coffeewithraghu@gmail.com](mailto:coffeewithraghu@gmail.com)) and include [Coffee] in the subject line, and we can work something out.

If you like anything about who I am or what I do, please do follow me or reach out to me. Don’t hesitate.
