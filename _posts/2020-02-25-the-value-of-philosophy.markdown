---
layout: post
title: The Value of Philosophy
comments: true
permalink: /the-value-of-philosophy/
categories: Philosophy
---




| Original Source:                                             |
| :----------------------------------------------------------- |
| See [XV: The value of philosophy](https://www.gutenberg.org/files/5827/5827-h/5827-h.htm#link2HCH0015) (from the '[_The problems of philosophy_](https://www.gutenberg.org/files/5827/5827-h/5827-h.htm)' by Bertrand Russell) |
| **Note**: If you want a more rigorous argument for the value of philosophy, I would recommend that you take a look at the wonderful book titled '[_Mathematical Philosophy_](http://www.gutenberg.org/files/41654/41654-pdf.pdf)' by _Bertrand Russell_, where he shows how philosophy gives rise to mathematics and vice-versa. |



And for those of you who feel too lazy to click the link and go through what Russell has to say, let me give you a small summary of his views here:

### The value of philosophy as Russell sees it

In the last chapter of this classic — "The problems of Philosophy" — Russell discusses the plight of the prejudice laden society by showing how the material-driven 'practical' men who in their quest to find the most *arithmetized* utilitarian system remain completely oblivious to the necessity of providing food for the mind. He argues that it is in this — the goods of the mind — lie the value of philosophy which he says is as much important as the goods of the body if not more in the construction of a society that can be called valuable in aggregate.

The beauty of philosophy he says lies not in studying from a reductionist point of view like the other forms of knowledge studies, but in forming a cohesive and unifying theory for those body of studies to stand upon, and help them examine critically the grounds of their conviction, prejudices, and beliefs.

If this sounds vague and too superficial an argument for you, wait until you realize that the sciences that you revere as the utmost form of truth is merely a re-incarnation of philosophy. The whole study of heavens, which now belongs to astronomy, was once included in philosophy; Newton's great work was called 'the mathematical principles of natural philosophy'. Similarly, the study of the human mind, which was a part of philosophy, has now been separated from philosophy and has become the science of psychology. Thus, to a great extent, the uncertainty of philosophy is more apparent than real: those questions which are already capable of definite answers are placed in the sciences, while those only to which, at present, no definite answer can be given, remain to form the residue which is called philosophy.

What is to be appreciated of philosophy here is its courage to ask the big questions first. The ability to take the first hit, whether it is the questions of what we now call sciences, or of the mystical nature like what does it mean to be conscious? Or what, if at all, is the purpose of your existence? Although the answers provided by philosophy to these questions seems to be demonstrably false, it tries with its every attempt to keep the hopes alive. It keeps alive that speculative interest in the universe which is apt to be killed by confining ourselves to definitely ascertainable knowledge.

He continues further that it is in these questions that the value must be sought; and it is this uncertainty that should show us how the certain, definite, and the obvious delude the best of us into believing in the finiteness of it, thus depriving us of the freedom from our own narrow focus with in the circle of our private interests. In such a life he says, there is something feverish and confined, in comparison with which the philosophic life is calm and free.

**Verbatim:** Thus, to sum up our discussion of the value of philosophy; Philosophy is to be studied, not for the sake of any definite answers to its questions, since no definite answers can, as a rule, be known to be true, but rather for the sake of the questions themselves; because these questions enlarge our conception of what is possible, enrich our intellectual imagination and diminish the dogmatic assurance which closes the mind against speculation; but above all because, through the greatness of the universe which philosophy contemplates, the mind also is rendered great, and becomes capable of that union with the universe which constitutes its highest good.

