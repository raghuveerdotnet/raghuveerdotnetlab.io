---
layout: post
title: How do I know if it is true?
comments: true
permalink: /how-do-i-know-if-it-is-true/
categories: Personal
---

Every morning you open the science column of a newspaper, you have a new research finding – I don’t remember clearly if it was last week that I saw an article on a prestigious newspaper citing some research conducted in a university in the USA proving the ill-effects of certain dietary restrictions and today morning to my surprise, the same newspaper cites another research conducted in a university in France stating such restrictions can reduce cholesterol levels by more than 40%. As for dietary recommendations, It doesn’t bother me much since I am going to keep eating what I want to but this raises a question in my mind – how do I know what is true?

I am not a researcher myself and I consume my information like everyone else from the internet, the newspaper, and the tv. And none of them seem to answer the question of how does one know if it is true. Just to clarify, it is not about fake news epidemic that is creating mob-lynchings and misunderstandings, it is about why should anyone ever believe in something because scientists said so. How different is it from a religion that asks you to take things on faith, meaning, if a week’s difference can produce two contradictory theories on such sensitive topics like dietary recommendations, which one do you go with and on what basis?

This doesn’t just stop at dietary recommendations, this is an epidemic that has plagued pretty much every field. String theorists have dedicated their lives to producing a cogent and coherent theory of everything, each varying slightly by several dimensions, and riddled with controversies like gazillion solutions, etc. No one seems to care much about it outside the physics community, but it is really disconcerting when it comes to the medical field. Not all of us can read a physics paper and concoct a story around it but almost all of us can read a psychology paper or some diluted report on that paper and confidently evaluate one’s mental health condition. Who is to say what is right? Who is to determine the effects of it?

One example that I can recall instantly is that of a friend of mine, who used to read papers on health and fitness; and used to advise us on what to eat to have a healthy life. Unfortunately, he was diagnosed with Non-alcoholic fatty liver due to excess protein intake. Although he is fine now, the question here is who should be blamed here? And now that he is afraid to follow such papers, who is at loss? This is not to say that scientists or modern-day researches aren’t good, it is instead about the question of how do we know what is true?

The most conspicuously vulnerable fields are the ones that are part of the social sciences group – economics, psychology, linguistics, anthropology, etc. It is too easy for an average joe like me to read a book on behavioral economics and become delusional about my powers as an influencer/manipulator. What does it say about me? A game theory ninja? What does it say about the theory in the book? Did it work already? Or it is just a gimmicky theory?

Thinking back, I feel it is the non-quantifiable fields(social sciences) that are so dangerous when they are freely available and not the quantifiable ones(hard sciences). I can’t say sht from shnola if you give me a complexity theory paper or a quantum physics paper but give me a report on evolution and you have a Darwinian policeman at your disposal.

I believe that there is something that can be done to reduce this mass misinformation. And it is simple, either codify the field as it is with hard sciences or just stop making it available to the public. The average joe is definitely not Richard Dawkins. The average joe is too primitive to contemplate on it, he will accept the theories proposed as is; and probably revere, and worship such scientists, and accept whatever they put out as a tenet. For what it matters, he will start influencing others to believe in it using his erudite charm and lampoon rest for not believing in it.

Nothing gives me more satisfaction than taking up one thing and understanding it to the core. I am not saying that everyone should be specialists, in fact, the more the generalists better is it for us but being a generalist does not mean gleaning tonnes and tonnes of information and regurgitating it back to look more confident. Being a generalist means knowing it and having some sort of experience that you can relate to the information that you have gathered.

This is too complicated for me to elucidate in a fashion that is devoid of controversy but what I definitely know is scientism is not same as science. Idolatry of scientists has inspired many to get into the field of science but worshipping what they say or write is equivalent to believing in the tenets of religion.

More importantly, it is not these scientists who are at fault, the real fault lies in whom I call the gist-makers – newspapers, online science magazines, clickbaity websites, etc – In fact, the research paper might have just originally said that the p-value is better for this treatment of leukemia, and the mass-media will broadcast it as cure for cancer. How do you know if it is true? What do you do when things go wrong? and More importantly, how do “I” know if it is true?

Probably, It is for our own good that we are always flooded with so much information. A recent paper (:P ) I read stated, when you are overwhelmed with choices, you go into a state of analysis-paralysis and don’t make decisions at all instead of making a bad one. Maybe, maybe not. Who is to say what is true?

Again, I really enjoyed writing this not because I was interested but because it gave me certain clarity delving into it and thinking about it. Hope you enjoyed reading it too.

As usual, enlighten me with a counter viewpoint(politely). Until then be happy, see ya later 🙂
