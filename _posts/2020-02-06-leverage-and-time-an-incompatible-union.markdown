---
layout: post
title: Leverage and time – An incompatible union?
comments: true
permalink: /leverage-and-time-an-incomptible-union/
categories: Practical
---

People say that we are in an age of infinite leverage, and I think it is true. Thanks in part to the technologies like internet and automobile, now we can do things in a fraction of a time of what it would have taken us, say a 1000 years ago, may it be communicating our ideas with the rest of the world in a jiffy, or going from one part of the world to the other within a day. And the technology is only going to get better, I am not saying if we will attain [singularity](https://en.wikipedia.org/wiki/Technological_singularity) or not, but we are definitely not going to go back to the caveman life. And this brings us to one of the most important of ideas in modern times — emergence. How much of this is temporally emergent? and what effect does it have on temporal emergence? (Self-Referentiality).

## Fat Tails and Temporality

Time as a dimension has always been different, in fact, it is unlike any other spatial dimensions in it can move only in one direction i.e., forward. Try to change the arrow of time and you’ll know. In fact, you don’t even need a complicated tool like quantum mechanics to understand it, just try to switch the tenses and interchange time with spatial dimensions in your statements, and you will understand how much of an effect does time have on our lives.

Here you go:

> “Yesterday at 3:00 am go up by 7 hours and take the previous 5 hours right, you found a small street that you will take to reach the day before yesterday after your auditions.”

Although the above example is ill-formed — and intentionally so — this should at least make you realize what the slightest change of any form to time can do(Moral: Don’t screw with time); this holds true even when you think about it philosophically. Time is the only genuine currency that we have for any kind of serious transactions. I say this because we have been placing tremendous value in the conventional form of currency a.k.a money(banknotes and coins), which is indeed useful for lots of what we want in life, but in other times it ceases to be useful for any kind of serious transaction, case in point being getting back the lost time. Time here is the only ideal currency to perform any meta-transactions, in essence, it is not just a currency, it is the ultimate meta-currency.

Time decides how our life changes and time and time only decides what lies ahead. No matter the amount of probability or the statistics one may know, there is no way one can predict the probability of the worst event to come. Let me explain.

Let us say, the current year is 1938 (one year before WWII), and you are equipped with all the statistical tools and knowledge that the mankind has ever developed, can you predict the death count of WWII? No, you can’t. All you can do is extrapolate the results of the historical events. But what about the discovery of the nuclear bomb(1945)? what if it had been used to nuke the entire world? Can you know that in advance?

_**The worst event is called worst because it has never happened before**_. And that is all the information that you can get from it, nothing more, nothing less. Yes, you can be prepared for an event of a smaller scale, or maybe the same scale, but if the event is going to be worse, it is going to be unpredictable. Remember, it is only in retrospect that we get to know that certain event was more fatal than the other, not apriori.

And this is precisely the reason for the mystical relation between the fat tails and the time. You cannot know the future, and unless your experiments are bounded both temporally and spatially there is no way to contain the risks, thus causing fatter and fatter tails.

A fine way to see this in effect would be to isolate the component of temporality in our day-to-day lives and notice how we are unnaturally speeding-up time with respect to certain processes in the name of leverage, whether it be technology, medicine, or finance. Take the situation of house building, for example, let’s say you want to build a new house and you don’t have sufficient money, what do you do? The organic/natural way of-course would be to save for a long time and build that house you wanted. But what do we do, we borrow loan from banks and speed up the time with respect to the process. And this kind of optimization/leverage has only created more complications where the natural process cannot work anymore i.e., what was natural earlier will now have to take into account the increasing housing prices due to inflation. It must be noted that I am not advocating for one over the other, I am not saying one is bad or the other is good, all I am saying is fat tails come with risks(unbounded). Some times if you do the right thing, time will reward you disproportionately, and at others, it will blow you up.

## Courage and Care

The thing to remember here is not that technology/development is good or bad, but that anything that you do should actually be bounded both spatially and temporally, and expand as you find more evidence for its contain-ability on a larger scale. Risk happens quicker, so think better before leveraging that leverage that you found.

The underlying notion of this essay is not to discourage people from taking risks, but in fact, it is the opposite i.e., to encourage people to take more risks. Most of us understand the risk that comes along when we do not upper bound our experiments spatially(see: the spatial and temporal bound in the previous paragraph), meaning, we never try to create a chain of restaurants like McDonalds right away, we try to build one locally and see the response and then expand based on the response. Similarly, we do not try out an uncontained nuclear explosion. But the problem is, we never tend to realize the effects of unbounded temporal experiments. Eg, what would happen 500 years later if I try this experiment today.

In fact, the advantage of bounded risk is just this, it encourages more risk taking due to its ability to contain losses, thus encouraging a society of risk-taking innovators. Do what you wish to do, but bound your experiments temporally as you would spatially. Take all the risks, but not at the expense of the future, for the future is not in your hands, and it is clearly unpredictable.

## How can you do it?

Do not be attached to an idea or entity because you invested twenty years of your life to it. If after twenty years of effort, if the risk still looks like a risk, there was no bound checking. It is as simple as this. The problem is either the risk will blow you up badly, or you will transfer this risk as time passes due to your insecurities(not good for the society — it is a chain reaction), and that is why I said, the sooner you realize the emergence of the bad kind of fat tails, the better it is.

<br/>

---

## References

Taleb, Nassim Nicholas. (2012). *Antifragile.*

Portesi, Paul. (2020). *Twitter(@paulportesi).*
> An excellent twitter handle where he churns out wisdom after wisdom, that will make you ponder for hours if not days. Do check it out. Most of the examples presented in this essay are paul's. So, do give his twitter account a visit if you happen to be on twitter. A very kind, likeable, and polite guy who will make you think.

<br/>