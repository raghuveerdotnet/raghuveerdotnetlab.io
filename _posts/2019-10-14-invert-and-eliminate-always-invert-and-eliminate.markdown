---
layout: post
title: Invert and Eliminate, Always Invert and Eliminate!
comments: true
permalink: /invert-and-eliminate-always-invert-and-eliminate/
categories: Philosophy Practical
---

In these many years, I have come across the idea of inversion in so many forms but never realized its value until recently when I had to make a big decision in my life. Everyone from the famed mathematician Carl Gustav Jacobi to legendary investor Charlie Munger have talked about inversion as the single most effective way to make a good decision. Inverting gives you the power to see a problem through the lens that you otherwise never could have. As humans we are conditioned to thinking in terms of looking for solutions to a problem in the way it is presented to us, but the moment you negate it you get this superpower to see what shouldn’t be done instead of what should be, which I believe is very difficult for most of us who think linearly. And when you couple this fantastic idea of inversion with the idea of elimination, you find yourself the magic wand to make the right decisions almost always.

Nassim Taleb, the renowned author of Black Swan and Skin in the game talks about elimination or more appropriately omission as a way to make robust decision that can help us avoid unintended consequences, which he calls _via negativa_ in his book “Skin in the game”. It is such a nifty mental model to keep in our mind that we can use any time and all the time. This compound model that I call _invert and eliminate_ is nothing new and most of us probably do it every day in one form or the other. What I wish to achieve through this article is if you can consciously use this every time you make a decision, this has the potential to improve your quality of life exponentially.

### But how do you invert and eliminate?

Ask yourself this simple question: What is the definition of being happy?

Is the definition universal? No. What gives happiness to one may not give happiness to others. For some, creating a billion dollar company might give happiness; For some, spending quality time with their family while working a decent paying job might give happiness. Whatever opinion you hold, the one thing that is clear is, the definition of happiness cannot be generalized. But we nonetheless are chasing it, aren’t we?

It is extremely difficult to get an answer for a question that is so subjective in nature, and what is more funny is majority of the decisions that we are forced to make in life are not analytical and universal, they are, in fact, subjective and confusing like the one with happiness.

Even if you were to ask yourself your own definition of being happy, it would still be difficult for you to answer that question as most of our desires are not our own, they are mimetic. We tend to decide what we want in our lives by looking at what others want.

Now try the opposite: What does it mean to be unhappy for me?

-   Ruminating over thoughts when the reality has already manifested itself
-   Reacting instead of responding
-   Comparing myself with my peers and other accomplished people
-   Spending more than saving
-   Being resentful and angry
-   Eating things despite knowing I shouldn’t

It is easier almost always to list things that you don’t want instead of things you want, for you will always want something or the other. The should do’s, the want to do’s and the to do’s is always a never ending list. Invert, Always Invert.

Chrysippus captures the unending want of ours beautifully through this quote:

> Wise people need many things, but they lack nothing. Fools need nothing, but they lack everything.
>
><cite>---Chrysippus</cite>

In my opinion, when you have to make a decision it is always a good heuristic to invert the question and eliminate all the answers that you get for that question. Most of the time we are inundated with so many choices in our lives that it becomes extremely difficult to make a good decision without being unsure of its side-effects. To be robust, go via negativa.

For what it matters, go one level above and create your own anti-list for just about everything and call it your principle. Don’t worry, it is much easier than you think. I have even written an entire [post](https://knowledgecontinuum.blog/2019/08/30/an-anti-guide-to-a-good-life-i/) about something similar — [an anti guide to a good life](https://knowledgecontinuum.blog/2019/08/30/an-anti-guide-to-a-good-life-i/). If this interested you, you might like that too.

Invert and Eliminate, Always Invert and Eliminate!
